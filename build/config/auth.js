"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const authConfig = {
    guard: 'web',
    guards: {
        web: {
            driver: 'session',
            provider: {
                driver: 'scylla',
            },
        },
    },
};
exports.default = authConfig;
//# sourceMappingURL=auth.js.map