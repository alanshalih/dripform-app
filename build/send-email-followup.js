"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const Scylla_1 = __importDefault(require("./app/Services/Scylla"));
Scylla_1.default.boot(function () {
    var query = {
        send_at: {
            '$token': { '$lt': Date.now().toString() }
        }
    };
    Scylla_1.default.models.instance.EmailFollowupQueue.find(query, function (err, data) {
        if (err) {
            throw err;
        }
        if (data.length) {
            let emails = [];
            data.forEach((item, index) => {
                Scylla_1.default.models.instance.Lead.findOne({ id: item.lead_id }, { raw: true, select: ["id", "status"] }, function (err, lead) {
                    if (err)
                        throw err;
                    if (lead.status == item.status) {
                        emails.push({
                            "from": item.sender_name + " <" + item.sender_email + ">",
                            "to": item.lead_email,
                            "transactional": true,
                            "subject": item.subject,
                            "html": item.content
                        });
                    }
                    item.delete(function (err) {
                        if (err)
                            throw err;
                        if (index == data.length - 1) {
                            axios_1.default.post(" http://172.104.180.206:2019/send-bulk", {
                                "data": emails,
                                "send_from": "drip.id"
                            }).then(() => {
                                setTimeout(() => {
                                    process.exit(0);
                                }, 3000);
                            });
                        }
                    });
                });
            });
        }
        else {
            process.exit(0);
        }
    });
});
//# sourceMappingURL=send-email-followup.js.map