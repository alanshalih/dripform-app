"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const Scylla_1 = __importDefault(require("./app/Services/Scylla"));
Scylla_1.default.boot(function () {
    var query = {
        send_at: {
            '$token': { '$lt': Date.now().toString() }
        }
    };
    Scylla_1.default.models.instance.WhatsappFollowupQueue.find(query, function (err, data) {
        if (err) {
            throw err;
        }
        if (data.length) {
            data.forEach((item, index) => {
                Scylla_1.default.models.instance.Lead.findOne({ id: item.lead_id }, { raw: true, select: ["id", "status"] }, function (err, lead) {
                    if (err)
                        throw err;
                    if (lead.status == item.status) {
                        let query = { text: item.content, phone: item.lead_phone, api_key: item.whatsapp_id };
                        if (item.hasButton) {
                            query.type = "buttonsMessage";
                            query.footerText = item.footerText;
                            query.buttons = item.buttons.map((item, index) => {
                                return { buttonId: 'id' + index, buttonText: { displayText: item }, type: 1 };
                            });
                        }
                        axios_1.default.post("http://api.dripsender.id/send", query).then(() => {
                            console.log('pesan dikirim');
                            item.delete(function (err) {
                                if (err)
                                    throw err;
                                if (index == data.length - 1) {
                                    setTimeout(() => {
                                        process.exit(0);
                                    }, 3000);
                                }
                            });
                        }, err => {
                            console.log(err);
                            item.delete(function (err) {
                                if (err)
                                    throw err;
                                if (index == data.length - 1) {
                                    setTimeout(() => {
                                        process.exit(0);
                                    }, 3000);
                                }
                            });
                        });
                    }
                    else {
                        item.delete(function (err) {
                            if (err)
                                throw err;
                            if (index == data.length - 1) {
                                setTimeout(() => {
                                    process.exit(0);
                                }, 3000);
                            }
                        });
                    }
                });
            });
        }
        else {
            process.exit(0);
        }
    });
});
//# sourceMappingURL=send-wa-followup.js.map