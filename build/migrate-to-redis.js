"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require('dotenv').config();
const redis = require("redis");
const client = redis.createClient({
    prefix: 'dripform:'
});
const Scylla_1 = __importDefault(require("./app/Services/Scylla"));
const a = Date.now();
Scylla_1.default.boot(() => {
    let tables = ["whatsapp-device", "whatsapp-followup", "email-followup", "moota-config", "tripay-config", "tripay-reference", "bank-transaction"];
    let models = ["WhatsappDevice", "WhatsappFollowup", "EmailFollowup", "BankToken", "Tripay", "TripayTransaction", "BankTransaction"];
    let pointer = 0;
    async function LoopFunction() {
        console.log(models[pointer]);
        Scylla_1.default.models.instance[models[pointer]].eachRow({}, { fetchSize: 50, raw: true }, function (_n, row) {
            if (row.id)
                row.id = row.id.toString();
            if (["whatsapp-device", "whatsapp-followup", "email-followup"].includes(tables[pointer])) {
                client.hset(tables[pointer] + ":" + row.form_id, row.id, JSON.stringify(row));
            }
            else if ("moota-config" == tables[pointer]) {
                client.set("moota-config:" + row.user_id, JSON.stringify(row));
            }
            else if ("tripay-config" == tables[pointer]) {
                client.set("tripay-config:" + row.user_id, JSON.stringify(row));
            }
            else if ("tripay-reference" == tables[pointer]) {
                client.setex("tripay-reference:" + row.reference, 86400, JSON.stringify(row));
            }
            else if ("bank-transaction" == tables[pointer]) {
                client.setex("bank-transaction:" + row.bank_account + row.price, 86400, JSON.stringify(row));
            }
        }, async function (err, result) {
            if (err)
                throw err;
            if (result.nextPage) {
                result.nextPage();
            }
            else {
                console.log("selesai insert table " + tables[pointer]);
                pointer++;
                if (pointer < tables.length) {
                    LoopFunction();
                }
                else {
                    const b = Date.now();
                    console.log('selesai dalam ' + ((b - a) / 1000) + ' s');
                    process.exit(1);
                }
            }
        });
    }
    LoopFunction();
});
//# sourceMappingURL=migrate-to-redis.js.map