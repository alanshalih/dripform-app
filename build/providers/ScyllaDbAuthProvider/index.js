"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ScyllaDbAuthProvider = void 0;
const Scylla_1 = __importDefault(global[Symbol.for('ioc.use')]("App/Services/Scylla"));
class ProviderUser {
    constructor(user, hash) {
        this.user = user;
        this.hash = hash;
    }
    getId() {
        return this.user ? this.user.id : null;
    }
    getRememberMeToken() {
        return this.user ? this.user.rememberMeToken : null;
    }
    setRememberMeToken(token) {
        if (!this.user) {
            return;
        }
        this.user.rememberMeToken = token;
    }
    async verifyPassword(plainPassword) {
        if (!this.user) {
            throw new Error('Cannot verify password for non-existing user');
        }
        return this.hash.verify(this.user.password, plainPassword);
    }
}
class ScyllaDbAuthProvider {
    constructor(config, hash) {
        this.config = config;
        this.hash = hash;
    }
    async getUserFor(user) {
        return new ProviderUser(user, this.hash);
    }
    async getUser(query) {
        return new Promise((resolve, reject) => {
            Scylla_1.default.models.instance.User.findOne(query, { raw: true }, function (err, people) {
                if (err) {
                    reject(err);
                    throw err;
                }
                resolve(people);
            });
        });
    }
    async updateRememberMeToken(user) {
        new Scylla_1.default.models.instance.User({
            id: user.getId(),
            rememberMeToken: user.getRememberMeToken()
        });
    }
    async findById(id) {
        const user = await this.getUser({ id: Scylla_1.default.models.uuidFromString(id) });
        return this.getUserFor(user || null);
    }
    async findByUid(uidValue) {
        const user = await this.getUser({ email: uidValue });
        return this.getUserFor(user || null);
    }
    async findByRememberMeToken(userId, token) {
        const user = await this.getUser({ id: userId, rememberMeToken: token });
        return this.getUserFor(user || null);
    }
}
exports.ScyllaDbAuthProvider = ScyllaDbAuthProvider;
//# sourceMappingURL=index.js.map