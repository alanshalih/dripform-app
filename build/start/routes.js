"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Route_1 = __importDefault(global[Symbol.for('ioc.use')]("Adonis/Core/Route"));
Route_1.default.post('register', 'AuthController.register');
Route_1.default.post('register-from-lp', 'AuthController.registerFromLP');
Route_1.default.post('login', 'AuthController.login');
Route_1.default.post('reset-password', 'AuthController.resetpassword');
Route_1.default.post('make-password', 'AuthController.makePassword');
Route_1.default.get('get-reset-token/:id', 'AuthController.verifyReset');
Route_1.default.get('/google/redirect', async ({ ally }) => {
    return ally.use('google').redirect();
});
Route_1.default.get('/google/callback', 'AuthController.googleRedirect');
Route_1.default.group(() => {
    Route_1.default.post('logout', 'AuthController.logout');
    Route_1.default.resource('/forms', 'FormsController');
    Route_1.default.put('/leads/update-status', 'LeadsController.updateStatus');
    Route_1.default.resource('/leads', 'LeadsController');
    Route_1.default.get('/leads/:id/downline', 'LeadsController.downline');
    Route_1.default.resource('/whatsapp-followup', 'WhatsappFollowupsController');
    Route_1.default.resource('/email-followup', 'EmailFollowupsController');
    Route_1.default.resource('/webhook-notification', 'WebhookNotificationsController');
    Route_1.default.resource('/locked-content', 'LockedContentsController');
    Route_1.default.resource('/whatsapp-device', 'WhatsappDevicesController');
    Route_1.default.get('/forms/:id/leaderboard', 'FormsController.leaderboard');
    Route_1.default.post('/domain-landing-page/:id', 'DomainsController.saveLandingPage');
    Route_1.default.resource('/domain', 'DomainsController');
    Route_1.default.get('/main-domain', 'DomainsController.mainDomain');
    Route_1.default.get('/setting/tripay', 'PaymentsController.tripay');
    Route_1.default.post('/setting/tripay', 'PaymentsController.tripaySubmit');
    Route_1.default.get('/setting/moota', 'PaymentsController.moota');
    Route_1.default.post('/setting/moota', 'PaymentsController.mootaSubmit');
    Route_1.default.get('/forms/:form_id/leads', 'LeadsController.index');
    Route_1.default.get('/forms/:form_id/admin', 'FormOwnersController.index');
    Route_1.default.post('/forms/:form_id/admin', 'FormOwnersController.store');
    Route_1.default.post('/forms/admin-delete', 'FormOwnersController.destroy');
    Route_1.default.get('/forms/checkslug/:slug', 'FormsController.checkSlug');
}).middleware('auth').prefix('/api');
Route_1.default.post("/tripay/callback", "PaymentsController.tripayCallback");
Route_1.default.post("/moota/callback", "PaymentsController.mootaCallback");
Route_1.default.get('*', async ({ view, auth }) => {
    try {
        await auth.use('web').authenticate();
        if (auth.user) {
            let user = auth.user;
            user.password = '';
            return view.render('welcome', { user });
        }
    }
    catch {
        return view.render('welcome');
    }
});
//# sourceMappingURL=routes.js.map