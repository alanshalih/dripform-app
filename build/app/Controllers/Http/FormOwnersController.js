"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Scylla_1 = __importDefault(global[Symbol.for('ioc.use')]("App/Services/Scylla"));
class FormOwnersController {
    async index({ params }) {
        const admins = await Scylla_1.default.models.instance.FormOwner.findAsync({ form_id: params.form_id }, { raw: true });
        const usersIds = admins.map(item => Scylla_1.default.models.uuidFromString(item.user_id));
        const users = await Scylla_1.default.models.instance.User.findAsync({ id: { '$in': usersIds } }, { raw: true, select: ["name", "id", "email"] });
        return { admins, users };
    }
    async create({}) {
    }
    async store({ params, request, response }) {
        const user = await Scylla_1.default.models.instance.User.findOneAsync({ email: request.input("email") });
        if (!user) {
            return response.status(404).send('User Not Found');
        }
        let formOwner = new Scylla_1.default.models.instance.FormOwner({ form_id: params.form_id, user_id: user.id.toString(), role: "admin" });
        await formOwner.saveAsync({ if_not_exist: true });
        return 'ok';
    }
    async show({}) {
    }
    async edit({}) {
    }
    async update({}) {
    }
    async destroy({ request, response }) {
        console.log(request.all());
        Scylla_1.default.models.instance.FormOwner.delete({ form_id: request.input("form_id"), user_id: request.input("user_id") }, function (err) {
            if (err) {
                return response.status(500).send('Unknown Error');
            }
            else {
                return response.send("ok");
            }
            ;
        });
    }
}
exports.default = FormOwnersController;
//# sourceMappingURL=FormOwnersController.js.map