"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Redis_1 = __importDefault(global[Symbol.for('ioc.use')]("Adonis/Addons/Redis"));
const Scylla_1 = __importDefault(global[Symbol.for('ioc.use')]("App/Services/Scylla"));
const TriggerAutomation_1 = require("../../Function/TriggerAutomation");
class LeadsController {
    async index({ params, request }) {
        let query = { form_id: { '$eq': params.form_id } };
        if (request.input('latest_fetch')) {
            query.updated = { '$gt': request.input('latest_fetch') };
        }
        let result = new Promise((resolve) => {
            let rows = [];
            let total = 0;
            Scylla_1.default.models.instance.Lead.eachRow(query, { materialized_view: 'updated_leads', fetchSize: 100, pageState: request.input('pageState') }, (n, row) => {
                total = n + 1;
                row.all_columns = JSON.parse(row.all_columns);
                rows.push(row);
            }, (err, result) => {
                if (err)
                    throw err;
                resolve({ pageState: result.pageState, data: rows, total });
            });
        });
        return result;
    }
    async create({}) {
    }
    async store({}) {
    }
    async show({ params }) {
        let lead = await Scylla_1.default.models.instance.Lead.findOneAsync({ id: params.id });
        lead.all_columns = JSON.parse(lead.all_columns);
        return lead;
    }
    async edit({}) {
    }
    async updateStatus({ request }) {
        let lead = await Scylla_1.default.models.instance.Lead.findOneAsync({ id: request.input('id') });
        lead.status = request.input("status");
        let all_columns = JSON.parse(lead.all_columns);
        all_columns.status = lead.status;
        lead.all_columns = JSON.stringify(all_columns);
        lead.updated = Date.now().toString();
        ;
        if (lead.status == 'dibayar') {
            lead.paid_at = Date.now().toString();
            lead.is_paid = true;
        }
        lead.save();
        lead.all_columns = all_columns;
        if (lead.whatsapp_device_id)
            (0, TriggerAutomation_1.TriggerWAAutomation)(lead, all_columns);
        if (lead.email)
            (0, TriggerAutomation_1.TriggerEmailAutomation)(lead, all_columns);
        return lead;
    }
    async update({ params, request }) {
        let lead = await Scylla_1.default.models.instance.Lead.findOneAsync({ id: params.id });
        let all_columns = request.input("all_columns");
        lead.all_columns = JSON.stringify(all_columns);
        lead.name = all_columns.name;
        lead.email = all_columns.email;
        lead.phone = all_columns.phone;
        lead.point = parseInt(request.input("point"));
        lead.updated = Date.now().toString();
        ;
        Redis_1.default.set("point:" + lead.id, lead.point);
        lead.save();
        return 'ok';
    }
    async destroy({}) {
    }
    async downline({ params }) {
        return await Scylla_1.default.models.instance.Lead.findAsync({ upline_id: params.id });
    }
}
exports.default = LeadsController;
//# sourceMappingURL=LeadsController.js.map