"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Scylla_1 = __importDefault(global[Symbol.for('ioc.use')]("App/Services/Scylla"));
class WhatsappFollowupsController {
    async index({ request }) {
        return Scylla_1.default.models.instance.WhatsappFollowup.findAsync({ form_id: request.input("form_id") });
    }
    async create({}) {
    }
    async store({ request }) {
        let reqData = request.all();
        reqData.id = Scylla_1.default.models.uuid();
        let data = new Scylla_1.default.models.instance.WhatsappFollowup(reqData);
        data.save();
        return reqData;
    }
    async show({}) {
    }
    async edit({}) {
    }
    async update({ request }) {
        Scylla_1.default.models.instance.WhatsappFollowup.update({ id: Scylla_1.default.models.uuidFromString(request.input('id')), form_id: request.input("form_id") }, request.except(['id', "form_id"]));
    }
    async destroy({ params }) {
        Scylla_1.default.models.instance.WhatsappFollowup.findOne({ id: Scylla_1.default.models.uuidFromString(params.id) }, function (err, data) {
            if (err)
                throw err;
            if (data)
                data.delete(function (err) {
                    if (err)
                        throw err;
                });
        });
    }
}
exports.default = WhatsappFollowupsController;
//# sourceMappingURL=WhatsappFollowupsController.js.map