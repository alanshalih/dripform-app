"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Scylla_1 = __importDefault(global[Symbol.for('ioc.use')]("App/Services/Scylla"));
const crypto = require('crypto');
const TriggerAutomation_1 = require("../../Function/TriggerAutomation");
class PaymentsController {
    async tripaySubmit({ auth, request }) {
        if (!auth.user) {
            return {};
        }
        let data = request.all();
        delete data.user_id;
        await Scylla_1.default.models.instance.Tripay.update({ user_id: auth.user.id }, data);
        return 'ok';
    }
    async tripay({ auth }) {
        if (!auth.user) {
            return {};
        }
        let tripay = await Scylla_1.default.models.instance.Tripay.findOneAsync({ user_id: auth.user.id });
        if (tripay) {
            return tripay;
        }
        else {
            tripay = { user_id: auth.user.id, urlCallback: process.env.APP_URL + '/tripay/callback' };
            let result = new Scylla_1.default.models.instance.Tripay(tripay);
            result.save();
            return tripay;
        }
    }
    async tripayCallback({ request }) {
        var data = request.all();
        if (!data.reference) {
            return 'no reference';
        }
        if (data.status != 'PAID') {
            return 'status not paid';
        }
        let transaction = await Scylla_1.default.models.instance.TripayTransaction.findOneAsync({ "reference": data.reference });
        if (transaction) {
            if (transaction.privateKey) {
                const HmacSHA256 = crypto.createHmac("sha256", transaction.privateKey);
                var signature = HmacSHA256.update(JSON.stringify(data)).digest("hex");
                let callbackSignature = request.header('X-Callback-Signature');
                if (callbackSignature != signature) {
                    return 'signature unmatched';
                }
            }
            let lead = await Scylla_1.default.models.instance.Lead.findOneAsync({ "id": transaction.lead_id });
            if (lead) {
                lead.is_paid = true;
                lead.paid_at = Date.now().toString();
                lead.status = 'dibayar';
                lead.updated = Date.now().toString();
                ;
                let all_columns = JSON.parse(lead.all_columns);
                all_columns.status = 'dibayar';
                lead.all_columns = JSON.stringify(all_columns);
                lead.save();
                if (lead.whatsapp_device_id)
                    (0, TriggerAutomation_1.TriggerWAAutomation)(lead, all_columns);
                if (lead.email)
                    (0, TriggerAutomation_1.TriggerEmailAutomation)(lead, all_columns);
            }
        }
        else {
            return 'no transaction';
        }
    }
    async mootaSubmit({ auth, request }) {
        if (!auth.user) {
            return {};
        }
        let data = request.all();
        delete data.user_id;
        await Scylla_1.default.models.instance.BankToken.update({ user_id: auth.user.id }, data);
        return 'ok';
    }
    async moota({ auth }) {
        if (!auth.user) {
            return {};
        }
        let moota = await Scylla_1.default.models.instance.BankToken.findOneAsync({ user_id: auth.user.id });
        if (moota) {
            return moota;
        }
        else {
            moota = { user_id: auth.user.id, token: (Math.random() + 1).toString(36).substring(2), urlCallback: process.env.APP_URL + '/moota/callback' };
            let result = new Scylla_1.default.models.instance.BankToken(moota);
            result.save();
            return moota;
        }
    }
    async mootaCallback({ request }) {
        var list_mutasi = request.body();
        for await (const mutasi of list_mutasi) {
            let transaction = await Scylla_1.default.models.instance.BankTransaction.findOneAsync({ "price": parseInt(mutasi.amount), bank_account: mutasi.account_number.toString() }, {});
            if (transaction) {
                if (transaction.token_id) {
                    const HmacSHA256 = crypto.createHmac("sha256", transaction.token_id);
                    var signature = HmacSHA256.update(JSON.stringify(list_mutasi)).digest("hex");
                    let callbackSignature = request.header('signature');
                    if (callbackSignature == signature) {
                        let lead = await Scylla_1.default.models.instance.Lead.findOneAsync({ "id": transaction.lead_id });
                        if (lead) {
                            lead.is_paid = true;
                            lead.paid_at = Date.now().toString();
                            lead.status = 'dibayar';
                            lead.updated = Date.now().toString();
                            ;
                            let all_columns = JSON.parse(lead.all_columns);
                            all_columns.status = 'dibayar';
                            lead.all_columns = JSON.stringify(all_columns);
                            lead.save();
                            transaction.delete();
                            if (lead.whatsapp_device_id)
                                (0, TriggerAutomation_1.TriggerWAAutomation)(lead, all_columns);
                            if (lead.email)
                                (0, TriggerAutomation_1.TriggerEmailAutomation)(lead, all_columns);
                        }
                    }
                }
            }
        }
        return 'ok';
    }
}
exports.default = PaymentsController;
//# sourceMappingURL=PaymentsController.js.map