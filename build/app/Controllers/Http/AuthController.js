"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Redis_1 = __importDefault(global[Symbol.for('ioc.use')]("Adonis/Addons/Redis"));
const Hash_1 = __importDefault(global[Symbol.for('ioc.use')]("Adonis/Core/Hash"));
const View_1 = __importDefault(global[Symbol.for('ioc.use')]("Adonis/Core/View"));
const axios_1 = __importDefault(require("axios"));
const Env_1 = __importDefault(global[Symbol.for('ioc.use')]("Adonis/Core/Env"));
const Scylla_1 = __importDefault(global[Symbol.for('ioc.use')]("App/Services/Scylla"));
class AuthController {
    async register({ request, auth, response }) {
        const user = request.only(["email", "password", "name", "id"]);
        var data = await Scylla_1.default.models.instance.User.findOneAsync({ email: user.email }, { raw: true });
        if (data) {
            return response.abort("Email telah digunakan", 409);
        }
        user.id = Scylla_1.default.models.uuid();
        user.password = await Hash_1.default.make(user.password);
        let getUser = await new Scylla_1.default.models.instance.User(user);
        await getUser.saveAsync();
        await auth.use('web').loginViaId(user.id.toString());
        delete user.password;
        return user;
    }
    async registerFromLP({ request, auth, response }) {
        const user = request.only(["email", "password", "name", "id"]);
        var data = await Scylla_1.default.models.instance.User.findOneAsync({ email: user.email }, { raw: true });
        if (data) {
            return response.abort("Email telah digunakan", 409);
        }
        user.id = Scylla_1.default.models.uuid();
        user.password = await Hash_1.default.make(user.password);
        let getUser = await new Scylla_1.default.models.instance.User(user);
        await getUser.saveAsync();
        await auth.use('web').loginViaId(user.id.toString());
        return response.redirect("/");
    }
    async logout({ auth }) {
        await auth.use('web').logout();
        return 'ok';
    }
    async login({ request, auth, response }) {
        const email = request.input('email');
        const password = request.input('password');
        var data = await Scylla_1.default.models.instance.User.findOneAsync({ email: email }, { raw: true });
        if (!data) {
            return response.abort("Email tidak ditemukan", 404);
        }
        await auth.attempt(email, password, true);
        return data;
        ;
    }
    async resetpassword({ request, response }) {
        const email = request.input('email');
        const user = await Scylla_1.default.models.instance.User.findOneAsync({ email: email }, {});
        if (user) {
            let code = Math.random().toString(36).substr(3, 35);
            await Redis_1.default.setex("reset-password:" + code, 3600, user.id);
            const html = await View_1.default.render('email/forgot-password', {
                name: user.name,
                unsubscribe_url: Env_1.default.get('APP_URL') + '/unsubscribe',
                RESET_URL: Env_1.default.get('APP_URL') + '/reset-password/' + code
            });
            axios_1.default.post('http://email.maxgrabb.com:2019/send-bulk', { data: [{
                        from: 'Reset Password Drip <arief@maxgrabb.com>',
                        to: user.email,
                        subject: 'Reset Password Drip',
                        html: html,
                        transactional: true
                    }], "send_from": "maxgrabb.com" });
            return 'ok';
        }
        else {
            return response.badRequest('Email Not Found');
        }
    }
    async verifyReset({ params, response }) {
        if (params.id) {
            const user_id = await Redis_1.default.get("reset-password:" + params.id);
            if (user_id) {
                return 'OK';
            }
            else {
                return response.badRequest('Cridential Not Found');
            }
        }
        else {
            return response.badRequest('Cridential Not Found');
        }
    }
    async makePassword({ request, response, auth }) {
        if (request.input('id')) {
            const user_id = await Redis_1.default.get("reset-password:" + request.input('id'));
            if (user_id) {
                const user = await Scylla_1.default.models.instance.User.findOneAsync({ id: Scylla_1.default.models.uuidFromString(user_id) });
                if (user) {
                    user.password = await Hash_1.default.make(request.input('password'));
                    user.save();
                    await auth.use('web').loginViaId(user.id.toString());
                    await Redis_1.default.del("reset-password:" + request.input('id'));
                    return user;
                }
                else {
                    return response.badRequest('Cridential Not Found');
                }
            }
            else {
                return response.badRequest('Cridential Not Found');
            }
        }
        else {
            return response.badRequest('Cridential Not Found');
        }
    }
    async googleRedirect({ ally, auth, response }) {
        const google = ally.use('google');
        if (google.accessDenied()) {
            return 'Access was denied';
        }
        if (google.stateMisMatch()) {
            return 'Request expired. Retry again';
        }
        if (google.hasError()) {
            return google.getError();
        }
        const googleuser = await google.user();
        let GetUser = await Scylla_1.default.models.instance.User.findOneAsync({ email: googleuser.email }, { raw: true });
        if (GetUser) {
            await auth.use('web').loginViaId(GetUser.id.toString());
            return response.redirect('/');
        }
        else {
            let user = {
                id: Scylla_1.default.models.uuid(),
                name: googleuser.name,
                email: googleuser.email,
                password: Math.random().toString(26)
            };
            user.password = await Hash_1.default.make(user.password);
            let getUser = await new Scylla_1.default.models.instance.User(user);
            await getUser.saveAsync();
            await auth.use('web').loginViaId(user.id.toString());
            return response.redirect('/');
        }
    }
}
exports.default = AuthController;
//# sourceMappingURL=AuthController.js.map