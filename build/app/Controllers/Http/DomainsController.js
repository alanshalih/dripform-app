"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Scylla_1 = __importDefault(global[Symbol.for('ioc.use')]("App/Services/Scylla"));
const axios_1 = __importDefault(require("axios"));
class DomainsController {
    async index({ auth }) {
        if (!auth.user) {
            return;
        }
        let domains = await Scylla_1.default.models.instance.Domain.findAsync({ main_domain: true }, { raw: true });
        if (domains.length == 0) {
            domains.push({
                name: process.env.FORM_DOMAIN,
                main_domain: true
            });
        }
        const userDomain = await Scylla_1.default.models.instance.Domain.findAsync({ user_id: auth.user.id.toString() }, { raw: true });
        domains = domains.concat(userDomain);
        return domains;
    }
    async create({}) {
    }
    async store({ request, response, auth }) {
        try {
            if (!auth.user) {
                return response.status(401).send("Tidak terauthentikasi");
            }
            let domainData = await axios_1.default.get('https://' + request.input('domain') + '/check-api', { timeout: 1000 });
            if (domainData.headers.server == 'cloudflare') {
                let data = {
                    id: Scylla_1.default.models.uuid(),
                    name: request.input("domain"),
                    user_id: auth.user.id.toString(),
                    main_domain: false
                };
                let domain = new Scylla_1.default.models.instance.Domain(data);
                domain.save();
                return response.send(data);
            }
            else {
                return response.status(401).send("Domain tidak terhubung");
            }
        }
        catch (error) {
            return response.status(401).send('Domain tidak terhubung');
        }
    }
    async show({}) {
    }
    async edit({}) {
    }
    async update({}) {
    }
    async destroy({ params }) {
        Scylla_1.default.models.instance.Domain.findOne({ id: Scylla_1.default.models.uuidFromString(params.id) }, function (err, data) {
            if (err)
                throw err;
            if (data)
                data.delete(function (err) {
                    if (err)
                        throw err;
                });
        });
    }
    async mainDomain() {
        let domain = await Scylla_1.default.models.instance.Domain.findOneAsync({ main_domain: true });
        if (domain)
            return domain.name;
        else
            return process.env.FORM_DOMAIN;
    }
    async saveLandingPage({ params, auth, response, request }) {
        if (!auth.user) {
            return response.status(404).send("tidak terauthentikasi");
        }
        try {
            let domain = await Scylla_1.default.models.instance.Domain.findOneAsync({ id: Scylla_1.default.models.uuidFromString(params.id) });
            domain.landing_page = request.input("landing_page");
            domain.save();
        }
        catch (error) {
            console.log(error);
            return response.status(404).send(error);
        }
    }
}
exports.default = DomainsController;
//# sourceMappingURL=DomainsController.js.map