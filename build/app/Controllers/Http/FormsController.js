"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Scylla_1 = __importDefault(global[Symbol.for('ioc.use')]("App/Services/Scylla"));
class FormsController {
    async index({ auth, response }) {
        if (!auth.user) {
            return response.abort("Tidak terauthentikasi");
        }
        const listForms = await Scylla_1.default.models.instance.FormOwner.findAsync({ user_id: auth.user.id.toString() }, { select: ['form_id'] });
        if (listForms.length == 0) {
            return [];
        }
        const forms = listForms.map(item => {
            return Scylla_1.default.models.uuidFromString(item.form_id);
        });
        let result = [];
        let promise = new Promise((resolve, reject) => {
            Scylla_1.default.models.instance.Form.eachRow({ id: { '$in': forms } }, { raw: true }, function (_n, row) {
                if (row)
                    result.push(row);
            }, function (err) {
                if (err) {
                    reject(err);
                    throw err;
                }
                ;
                resolve(result);
            });
        });
        return await promise;
        return result;
    }
    async create({}) {
    }
    async leaderboard({ request, params }) {
        const limit = parseInt(request.input('limit')) || 10;
        let data = await Scylla_1.default.models.instance.Lead.findAsync({ form_id: { '$eq': params.id }, $orderby: { '$desc': 'point' }, $limit: limit }, { materialized_view: 'leaderboard', raw: true });
        return data;
    }
    async store({ request }) {
        let data = request.all();
        data.id = Scylla_1.default.models.uuid();
        data.header = JSON.stringify(data.header);
        data.response_fields = JSON.stringify(data.response_fields);
        data.list_field = JSON.stringify(data.list_field);
        data.trx_status = JSON.stringify(data.trx_status);
        data.created = Date.now().toString();
        data.updated = Date.now().toString();
        let formOwner = new Scylla_1.default.models.instance.FormOwner({ form_id: data.id.toString(), user_id: data.user_id, role: "owner" });
        await formOwner.saveAsync();
        let form = new Scylla_1.default.models.instance.Form(data);
        await form.saveAsync();
        await Scylla_1.default.models.instance.FormOwner.findOneAsync({ form_id: data.id.toString(), user_id: data.user_id });
        return data;
    }
    async show({ params, auth, response }) {
        if (!auth.user) {
            return response.abort("Tidak terauthentikasi");
        }
        let formOwner = await Scylla_1.default.models.instance.FormOwner.findOneAsync({ form_id: params.id, user_id: auth.user.id.toString() });
        if (!formOwner) {
            return response.abort("Tidak terauthentikasi");
        }
        let form = await Scylla_1.default.models.instance.Form.findOneAsync({ id: Scylla_1.default.models.uuidFromString(params.id) }, { raw: true });
        form.header = JSON.parse(form.header);
        form.response_fields = JSON.parse(form.response_fields);
        form.list_field = JSON.parse(form.list_field);
        if (form.trx_status) {
            form.trx_status = JSON.parse(form.trx_status);
        }
        else {
            if (form.is_ecommerce) {
                form.trx_status = ["dipesan", "dibayar", "diproses", "dikirim", "selesai"];
            }
            else {
                form.trx_status = ['submit'];
            }
        }
        return form;
    }
    async edit({}) {
    }
    async update({ params, request }) {
        let data = request.except(['id']);
        data.header = JSON.stringify(data.header);
        data.list_field = JSON.stringify(data.list_field);
        data.trx_status = JSON.stringify(data.trx_status);
        data.response_fields = JSON.stringify(data.response_fields);
        data.complete_url = data.domain + '/' + data.slug;
        data.updated = Date.now().toString();
        data.lead_point = parseInt(data.lead_point);
        delete data.lead_number;
        await Scylla_1.default.models.instance.Form.update({ id: Scylla_1.default.models.uuidFromString(params.id) }, data);
        return 'ok';
    }
    async checkSlug({ params }) {
        console.log(params.slug);
        return await Scylla_1.default.models.instance.Form.findOneAsync({
            slug: params.slug
        }, { select: ['slug'], raw: true });
    }
    async destroy({ params }) {
        Scylla_1.default.models.instance.Form.delete({
            id: Scylla_1.default.models.uuidFromString(params.id)
        });
        var leads = await Scylla_1.default.models.instance.Lead.findAsync({ form_id: params.id }, {});
        leads.forEach(element => {
            element.delete();
        });
        return 'ok';
    }
}
exports.default = FormsController;
//# sourceMappingURL=FormsController.js.map