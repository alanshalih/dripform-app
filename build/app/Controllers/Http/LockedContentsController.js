"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Scylla_1 = __importDefault(global[Symbol.for('ioc.use')]("App/Services/Scylla"));
class LockedContentsController {
    async index({}) {
    }
    async create({}) {
    }
    async store({ request }) {
        let data = request.all();
        data.id = Scylla_1.default.models.uuid();
        data.point = parseInt(data.point);
        let result = new Scylla_1.default.models.instance.LockedContent(data);
        result.save();
        return data;
    }
    async show({ params }) {
        return await Scylla_1.default.models.instance.LockedContent.findOneAsync({ id: Scylla_1.default.models.uuidFromString(params.id) });
    }
    async edit({}) {
    }
    async update({ params, request }) {
        let data = request.except(["id"]);
        data.point = parseInt(data.point);
        Scylla_1.default.models.instance.LockedContent.update({ id: Scylla_1.default.models.uuidFromString(params.id) }, data);
    }
    async destroy({ params }) {
        Scylla_1.default.models.instance.LockedContent.delete({ id: Scylla_1.default.models.uuidFromString(params.id) });
    }
}
exports.default = LockedContentsController;
//# sourceMappingURL=LockedContentsController.js.map