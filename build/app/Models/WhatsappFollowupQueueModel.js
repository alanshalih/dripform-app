module.exports = {
    fields: {
        id: 'uuid',
        form_id: 'varchar',
        content: "text",
        title: 'varchar',
        send_at: 'varchar',
        status: 'varchar',
        whatsapp_id: 'varchar',
        whatsapp_device_id: 'varchar',
        sender_name: 'varchar',
        sender_phone: 'varchar',
        lead_id: 'varchar',
        lead_name: 'varchar',
        lead_phone: 'varchar',
        tries: {
            type: 'int',
            default: 0
        },
        hasButton: 'boolean',
        footerText: 'text',
        buttons: {
            type: "set",
            typeDef: "<varchar>"
        },
    },
    key: [["send_at"], 'id'],
    indexes: ["form_id"]
};
//# sourceMappingURL=WhatsappFollowupQueueModel.js.map