module.exports = {
    fields: {
        id: "uuid",
        name: "text",
        password: "text",
        phone: "text",
        email: "text",
        rememberMeToken: "text",
        plan: "varchar",
        membership_expired: "varchar",
        created: {
            type: "timestamp",
            default: { "$db_function": "toTimestamp(now())" }
        }
    },
    key: ["id"],
    indexes: ["email"]
};
//# sourceMappingURL=UserModel.js.map