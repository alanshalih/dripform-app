module.exports = {
    fields: {
        id: "uuid",
        name: "text",
        user_id: "varchar",
        landing_page: 'varchar',
        main_domain: {
            type: 'boolean',
            default: false
        }
    },
    key: ["id"],
    indexes: ["main_domain", "user_id", "name"]
};
//# sourceMappingURL=DomainModel.js.map