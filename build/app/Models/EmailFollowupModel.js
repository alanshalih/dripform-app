module.exports = {
    fields: {
        id: 'uuid',
        form_id: 'varchar',
        content: "text",
        title: 'varchar',
        subject: 'varchar',
        send_after: 'varchar',
        send_after_unit: 'varchar',
        status: 'varchar',
        sender_name: 'varchar',
        sender_email: 'varchar'
    },
    key: [["form_id"], "id"]
};
//# sourceMappingURL=EmailFollowupModel.js.map