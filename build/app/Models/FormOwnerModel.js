module.exports = {
    fields: {
        form_id: 'varchar',
        user_id: "varchar",
        role: "varchar"
    },
    key: [["user_id"], "form_id"],
    indexes: ["form_id"]
};
//# sourceMappingURL=FormOwnerModel.js.map