module.exports = {
    fields: {
        user_id: "uuid",
        token: "text",
        urlCallback: "text",
    },
    key: ["user_id"]
};
//# sourceMappingURL=BankTokenModel.js.map