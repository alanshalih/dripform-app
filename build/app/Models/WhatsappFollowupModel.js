module.exports = {
    fields: {
        id: 'uuid',
        form_id: 'varchar',
        content: "text",
        title: 'varchar',
        send_after: 'varchar',
        send_after_unit: 'varchar',
        status: 'varchar',
        hasButton: 'boolean',
        type: 'varchar',
        footerText: 'text',
        buttons: {
            type: "set",
            typeDef: "<varchar>"
        },
    },
    key: [["form_id"], "id"]
};
//# sourceMappingURL=WhatsappFollowupModel.js.map