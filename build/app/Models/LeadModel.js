module.exports = {
    fields: {
        id: 'varchar',
        name: "varchar",
        email: "varchar",
        phone: "varchar",
        form_id: "varchar",
        lead_id: 'varchar',
        upline_id: 'varchar',
        affiliate_id: 'varchar',
        all_columns: 'text',
        is_paid: {
            type: 'boolean',
            default: false
        },
        paid_at: "varchar",
        province: "varchar",
        city: "varchar",
        city_type: "varchar",
        subdistrict_name: "varchar",
        subdistrict_id: "varchar",
        courier_code: "varchar",
        courier_name: "varchar",
        courier_service: "varchar",
        payment_method: "varchar",
        payment_gateway: "varchar",
        whatsapp_device_id: "varchar",
        payment_data: "text",
        point: {
            type: 'int',
            default: 0
        },
        lead_number: {
            type: 'int',
            default: 0
        },
        status: {
            type: 'varchar',
            default: 'submit'
        },
        created: 'varchar',
        updated: 'varchar'
    },
    key: [["id"], "form_id"],
    indexes: ['form_id', 'upline_id'],
    materialized_views: {
        leaderboard: {
            select: ["name", "email", "phone", "point", "id", "form_id", "updated"],
            key: ["form_id", "point", "id"],
        },
        updated_leads: {
            select: ["id", "name", "email", "phone", "form_id", "all_columns", "is_paid", "point", "status", "created", "updated", "payment_method", "payment_gateway", "lead_number", "upline_id"],
            key: ["form_id", "updated", "id"],
        },
        lead_by_phone: {
            select: ["id", "phone", "form_id"],
            key: ["form_id", "phone", "id"],
        },
        lead_by_email: {
            select: ["id", "email", "form_id"],
            key: ["form_id", "email", "id"],
        },
    }
};
//# sourceMappingURL=LeadModel.js.map