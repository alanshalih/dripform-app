module.exports = {
    fields: {
        id: 'uuid',
        form_id: 'varchar',
        whatsapp_id: "varchar",
        phone: 'varchar',
        name: 'varchar'
    },
    key: [["form_id"], "id"]
};
//# sourceMappingURL=WhatsappDeviceModel.js.map