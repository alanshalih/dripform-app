module.exports = {
    fields: {
        id: 'uuid',
        title: "varchar",
        slug: "varchar",
        domain: "varchar",
        "complete_url": {
            "type": "varchar",
            default: function () {
                return this.domain + '/' + this.slug;
            }
        },
        lead_point: {
            type: 'int',
            default: 20
        },
        lead_number: {
            type: 'int',
            default: 0
        },
        hit_counter: {
            type: 'int',
            default: 0
        },
        leaderboard_limit: {
            type: 'int',
            default: 50
        },
        is_ecommerce: {
            type: 'boolean',
            default: false
        },
        user_id: "varchar",
        socmed_title: 'varchar',
        socmed_description: 'varchar',
        socmed_thumbnail: 'varchar',
        active_session: 'boolean',
        unique_identifier: 'varchar',
        color_schema: 'varchar',
        response_fields: 'text',
        set_status: {
            type: 'set',
            typeDef: '<text>',
        },
        trx_status: 'varchar',
        header: 'text',
        list_field: 'text',
        body_script: 'text',
        head_script: 'text',
        is_pro: {
            type: 'boolean',
            default: false
        },
        wa_automation: {
            type: 'boolean',
            default: false
        },
        email_automation: {
            type: 'boolean',
            default: false
        },
        webhook_automation: {
            type: 'boolean',
            default: false
        },
        obs_automation: {
            type: 'boolean',
            default: false
        },
        created: 'varchar',
        updated: 'varchar'
    },
    key: [["id"]],
    indexes: ['slug', 'user_id']
};
//# sourceMappingURL=FormModel.js.map