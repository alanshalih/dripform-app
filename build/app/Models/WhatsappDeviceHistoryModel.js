module.exports = {
    fields: {
        user_id: 'varchar',
        whatsapp_id: "varchar",
        phone: 'varchar',
        name: 'varchar'
    },
    key: [["user_id"], "whatsapp_id"]
};
//# sourceMappingURL=WhatsappDeviceHistoryModel.js.map