module.exports = {
    fields: {
        id: "uuid",
        status: {
            type: "set",
            typeDef: "<varchar>"
        },
        content: "text",
        point: "int"
    },
    key: ["id"]
};
//# sourceMappingURL=LockedContentModel.js.map