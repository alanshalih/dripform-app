"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var models = require('express-cassandra');
require('dotenv').config();
let host = process.env.SCYLLA_HOST;
let array_host;
if (host) {
    array_host = host.split(',');
}
let clientOptions = {
    contactPoints: array_host,
    localDataCenter: process.env.SCYLLA_DC,
    keyspace: process.env.SCYLLA_KEYSPACE,
    queryOptions: { consistency: models.consistencies.one }
};
function boot(callback) {
    models.setDirectory(__dirname + '/../Models').bind({
        clientOptions: clientOptions,
        ormOptions: {
            defaultReplicationStrategy: {
                class: 'SimpleStrategy',
                replication_factor: array_host.length,
            },
            disableTTYConfirmation: true,
            migration: 'safe',
            createKeyspace: false,
        }
    }, function (err) {
        if (callback) {
            callback();
        }
        if (err)
            throw err;
    });
}
function migration() {
    models.setDirectory(__dirname + '/../Models').bind({
        clientOptions: clientOptions,
        ormOptions: {
            defaultReplicationStrategy: {
                class: 'SimpleStrategy',
                replication_factor: array_host.length,
            },
            disableTTYConfirmation: false,
            migration: 'alter',
            createKeyspace: true,
        }
    }, function (err) {
        if (err)
            throw err;
    });
}
function development() {
    models.setDirectory(__dirname + '/../Models').bind({
        clientOptions: clientOptions,
        ormOptions: {
            defaultReplicationStrategy: {
                class: 'SimpleStrategy',
                replication_factor: array_host.length,
            },
            disableTTYConfirmation: true,
            migration: 'alter',
            createKeyspace: true,
        }
    }, function (err) {
        if (err)
            throw err;
    });
}
exports.default = { models, boot, migration, development };
//# sourceMappingURL=Scylla.js.map