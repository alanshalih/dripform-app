"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TriggerEmailAutomation = exports.TriggerWAAutomation = void 0;
const Scylla_1 = __importDefault(global[Symbol.for('ioc.use')]("App/Services/Scylla"));
const dayjs = require('dayjs');
function TriggerWAAutomation(lead, all_columns) {
    Scylla_1.default.models.instance.WhatsappDevice.findOne({ form_id: lead.form_id, id: Scylla_1.default.models.uuidFromString(lead.whatsapp_device_id) }, { raw: true }, function (err, device) {
        if (err)
            throw err;
        if (device) {
            Scylla_1.default.models.instance.WhatsappFollowup.find({ form_id: lead.form_id }, { raw: true }, function (err, rules) {
                if (err)
                    throw err;
                if (rules) {
                    rules.forEach(item => {
                        if (item.status == lead.status && lead.whatsapp_device_id) {
                            let data = item;
                            data.id = Scylla_1.default.models.uuid();
                            Object.keys(all_columns).forEach(key => {
                                if (key) {
                                    if (['donasi', 'total', 'subtotal', 'courier_cost'].includes(key)) {
                                        data.content = data.content.split('[' + key + ']').join(parseInt(all_columns[key]).toLocaleString('id'));
                                    }
                                    else {
                                        data.content = data.content.split('[' + key + ']').join(all_columns[key]);
                                    }
                                }
                            });
                            data.whatsapp_id = device.whatsapp_id;
                            data.whatsapp_device_id = device.id.toString();
                            data.sender_name = device.name;
                            data.send_at = dayjs().add(item.send_after, item.send_after_unit).valueOf().toString();
                            data.lead_id = lead.id;
                            data.lead_name = lead.name;
                            data.lead_phone = lead.phone;
                            let queue = new Scylla_1.default.models.instance.WhatsappFollowupQueue(data);
                            queue.save(function (err) {
                                if (err)
                                    throw err;
                            });
                        }
                    });
                }
            });
        }
    });
}
exports.TriggerWAAutomation = TriggerWAAutomation;
function TriggerEmailAutomation(lead, all_columns) {
    if (lead.email) {
        Scylla_1.default.models.instance.EmailFollowup.find({ form_id: lead.form_id }, { raw: true }, function (err, rules) {
            if (err)
                throw err;
            if (rules) {
                rules.forEach(item => {
                    if (item.status == lead.status) {
                        let data = item;
                        data.id = Scylla_1.default.models.uuid();
                        Object.keys(all_columns).forEach(key => {
                            if (key) {
                                if (['donasi', 'total', 'subtotal', 'courier_cost'].includes(key)) {
                                    data.content = data.content.split('[' + key + ']').join(parseInt(all_columns[key]).toLocaleString('id'));
                                    data.subject = data.subject.split('[' + key + ']').join(parseInt(all_columns[key]).toLocaleString('id'));
                                }
                                else {
                                    data.content = data.content.split('[' + key + ']').join(all_columns[key]);
                                    data.subject = data.subject.split('[' + key + ']').join(all_columns[key]);
                                }
                            }
                        });
                        data.sender_name = item.sender_name;
                        data.send_at = dayjs().add(item.send_after, item.send_after_unit).valueOf().toString();
                        data.lead_id = lead.id;
                        data.lead_name = lead.name;
                        data.lead_email = lead.email;
                        let queue = new Scylla_1.default.models.instance.EmailFollowupQueue(data);
                        queue.save(function (err) {
                            if (err)
                                throw err;
                        });
                    }
                });
            }
        });
    }
}
exports.TriggerEmailAutomation = TriggerEmailAutomation;
//# sourceMappingURL=TriggerAutomation.js.map