var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB || window.shimIndexedDB;
class IndexDBHandler { 

    
   

    constructor() {  
       
        if(this.boot)
        {
            return;
        }
        
      
        this._events = {}; 
        this.open = indexedDB.open("DatabaseDripForm"); 
        this.data = {};
        
      
        this.open.onsuccess = (event)=> {
            this.emit('init') 
            this.boot = true;
        };

     

    }


    get(collection){
 
        if(this.data[collection])
        {
            
            this.emit('data-loaded',{collection, data : this.data[collection]}) 
        }else{

            var db = this.open.result;  
            
            if(db.objectStoreNames.contains(collection))
            {
                var tx = db.transaction(collection, "readwrite"); 
                var store = tx.objectStore(collection);
                var request = store.getAll(); 

                request.onsuccess = ()=>{    
                    this.data[collection] = request.result;
                
                    this.emit('data-loaded',{collection, data : this.data[collection]}) 
                }
                return [];
            }else{
                const version = parseInt(db.version);
                db.close();
                
                this.open = indexedDB.open("DatabaseDripForm", version+1); 
        
                this.open.onupgradeneeded = (event)=> {  
                
                    event.target.result.createObjectStore(collection, { keyPath: "id" });
                    this.data[collection] = [];
                    this.emit('collection-created',{collection})   
                };  
                return [];

            }
            
            
          

        }
        
    }

    add(collection,item){ 
        this.data[collection].unshift(item);
        var db = this.open.result;
        var tx = db.transaction(collection, "readwrite");
        var store = tx.objectStore(collection);
        store.add(item);  
    } 
    update(collection,item){ 
        var db = this.open.result;
        var tx = db.transaction(collection, "readwrite");
        var store = tx.objectStore(collection);
        store.put(item);  
    }

    emit(name, data) { 
        if (!this._events[name]) {
          console.log(`Can't emit an event. Event "${name}" doesn't exits.`);
          return;
        }
    
        const fireCallbacks = (callback) => {
          callback(data);
        };
    
        this._events[name].forEach(fireCallbacks);
    }
    on(name, listener) {
        if (!this._events[name]) {
            this._events[name] = [];
        }

        this._events[name].push(listener);
    }
    destroyEvent(name)
    {
        delete this._events[name];

    }

}

export default (new IndexDBHandler);