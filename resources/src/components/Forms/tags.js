export default  function tags(data) {
    let tag = ['id', 'point'];
    data.list_field.forEach(item => {
      if (item.header == 'single') {
        tag.push(item.name)
      } else if (item.header == 'special') {
        if (item.type == 'donasi') {
          tag.push('donasi')
          tag.push('status')
        }
        if (item.type == 'produk') {
          tag.push('total')
          tag.push('subtotal')
          tag.push('courier_name')
          tag.push('courier_service')
          tag.push('courier_cost')
          tag.push('no_resi')
          tag.push('status')

        }
        if (item.type == 'kecamatan') {
          tag.push('province')
          tag.push('city_type')
          tag.push('city')
          tag.push('subdistrict_name')

        }
      }
    })
    return tag;
  }