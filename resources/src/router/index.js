import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home'
import NotFound from '../views/NotFound'
import Layout from '../views/Layout'
import store from '../store'

const routes = [
  {
    path: '/',
    component: Layout,
    meta: {
      requiresAuth: true
    },
    children : [
      {
        // Document title tag
        // We combine it with defaultDocumentTitle set in `src/main.js` on router.afterEach hook
        meta: {
          title: 'Dashboard'
        },
        path: '/',
        name: 'home',
        component: Home
      },
      {
        meta: {
          title: 'Tables'
        },
        path: '/tables',
        name: 'tables',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "tables" */ '../views/Tables')
      },
      {
        meta: {
          title: 'Latihan'
        },
        path: '/latihan',
        name: 'latihan',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "tables" */ '../views/Latihan')
      },
      {
        meta: {
          title: 'Forms'
        },
        path: '/forms',
        name: 'forms',
        component: () => import(/* webpackChunkName: "forms" */ '../views/IndexForm')
      },{
        meta: {
          title: 'Payment'
        },
        path: '/payment',
        name: 'payment',
        component: () => import(/* webpackChunkName: "forms" */ '../views/Payment')
      },{
        meta: {
          title: 'Domain'
        },
        path: '/domain',
        name: 'domain',
        component: () => import(/* webpackChunkName: "forms" */ '../views/Domain')
      },
      {
        meta: {
          title: 'Create Form'
        },
        path: '/create-form',
        name: 'create-form',
        component: () => import(/* webpackChunkName: "forms" */ '../views/CreateForm')
      },
      {
        meta: {
          title: 'Edit Form'
        },
        path: '/edit-form/:id',
        name: 'edit-form',
        component: () => import(/* webpackChunkName: "forms" */ '../views/EditForm')
      },
      {
        meta: {
          title: 'Lead'
        },
        path: '/forms/:id/leads',
        name: 'index-lead',
        component: () => import(/* webpackChunkName: "forms" */ '../views/IndexLead')
      },
      {
        meta: {
          title: 'Lead'
        },
        path: '/forms/:id/analyze-lead',
        name: 'analyze-lead',
        component: () => import(/* webpackChunkName: "forms" */ '../views/AnalyzeLead')
      },
      {
        meta: {
          title: 'Lead'
        },
        path: '/forms/:id/leads/:lead_id',
        name: 'edit-lead',
        component: () => import(/* webpackChunkName: "forms" */ '../views/EditLead')
      },
      {
        meta: {
          title: 'Downline'
        },
        path: '/forms/:id/leads/:lead_id/downline',
        name: 'downline-lead',
        component: () => import(/* webpackChunkName: "forms" */ '../views/LeadDownline')
      },{
        meta: {
          title: 'Leaderboard'
        },
        path: '/forms/:id/leaderboard',
        name: 'leaderboard',
        component: () => import(/* webpackChunkName: "forms" */ '../views/Leaderboard')
      },
      {
        meta: {
          title: 'Profile'
        },
        path: '/profile',
        name: 'profile',
        component: () => import(/* webpackChunkName: "profile" */ '../views/Profile')
      },
    ]
  },
  {
    meta: {
      title: 'Login',
      redirectIfAuthenticated : true
    },
    path: '/login',
    name: 'login',
    component: () => import(/* webpackChunkName: "login" */ '../views/Login')
  },
  {
    meta: {
      title: 'register',
      redirectIfAuthenticated : true
    },
    path: '/register',
    name: 'register',
    component: () => import(/* webpackChunkName: "register" */ '../views/Register')
  },
  {
    meta: {
      title: 'Forgot Password',
      redirectIfAuthenticated : true
    },
    path: '/forgot-password',
    component: () => import(/* webpackChunkName: "register" */ '../views/ForgotPassword')
  },
  {
    meta: {
      title: 'Reset Password',
      redirectIfAuthenticated : true
    },
    path: '/reset-password/:id',
    component: () => import(/* webpackChunkName: "register" */ '../views/ResetPassword')
  },
  { path: '/not-found', name: 'not-found', component: NotFound },
  { path: '/not-authorize', name: 'not-authorize', component: import(/* webpackChunkName: "register" */ '../views/NotAuthorize') },
]

const router = createRouter({
  history: createWebHistory(),
  routes,
  scrollBehavior (to, from, savedPosition) {
    return savedPosition || { x: 0, y: 0 }
  }
})

router.beforeEach((to, from, next) => { 
 
  if (to.matched.some(record => record.meta.requiresAuth)) { 
    if (store.state.isAuth) {
      store.dispatch('formScreenToggle', false)
          next();
    } else {
        next('/login');
    }
  }else if (to.matched.some(record => record.meta.redirectIfAuthenticated)) { 
    if (store.state.isAuth) {
          next('/');
    } else { 
        store.dispatch('formScreenToggle', true)
        next();
    }
  }else {  
    store.dispatch('formScreenToggle', true)
    if(to.path == '/not-found' || to.path == '/not-authorize')
    next();
    else
      next('/not-found') // make sure to always call next()! 
  } 
});

export default router
