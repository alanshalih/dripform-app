export default  {
    _events : {},
     emit : function(name, data) { 
        if (!this._events[name]) {
          console.log(`Can't emit an event. Event "${name}" doesn't exits.`);
          return;
        }
    
        const fireCallbacks = (callback) => {
          callback(data);
        };
    
        this._events[name].forEach(fireCallbacks);
    },
    on : function(name, listener) {
        if (!this._events[name]) {
            this._events[name] = [];
        }

        this._events[name].push(listener);
    }
}