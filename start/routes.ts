/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer''
|
*/

import Route from '@ioc:Adonis/Core/Route'   

Route.post('register','AuthController.register');

Route.post('register-from-lp','AuthController.registerFromLP');

Route.post('login','AuthController.login');

Route.post('reset-password','AuthController.resetpassword');

Route.post('make-password','AuthController.makePassword');

Route.get('get-reset-token/:id','AuthController.verifyReset');

 
Route.get('/google/redirect', async ({ ally }) => {
  return ally.use('google').redirect()
})


 

Route.get('/google/callback', 'AuthController.googleRedirect')


Route.group(() => {
  // All routes here are part of the group
  // all logic came here
  Route.post('logout','AuthController.logout');

  Route.resource('/forms','FormsController');

  Route.put('/leads/update-status','LeadsController.updateStatus');

  Route.resource('/leads','LeadsController');

  Route.get('/leads/:id/downline','LeadsController.downline');

  Route.resource('/whatsapp-followup','WhatsappFollowupsController');

  Route.resource('/email-followup','EmailFollowupsController');

  Route.resource('/webhook-notification','WebhookNotificationsController');

  Route.resource('/locked-content','LockedContentsController');

  Route.resource('/whatsapp-device','WhatsappDevicesController');

  Route.get('/forms/:id/leaderboard','FormsController.leaderboard');

  Route.post('/domain-landing-page/:id','DomainsController.saveLandingPage');

  Route.resource('/domain','DomainsController');

  Route.get('/main-domain','DomainsController.mainDomain');

  Route.get('/setting/tripay','PaymentsController.tripay');

  Route.post('/setting/tripay','PaymentsController.tripaySubmit');

  Route.get('/setting/moota','PaymentsController.moota');

  Route.post('/setting/moota','PaymentsController.mootaSubmit');

  Route.get('/forms/:form_id/leads','LeadsController.index');

 

  Route.get('/forms/:form_id/admin','FormOwnersController.index');

  Route.post('/forms/:form_id/admin','FormOwnersController.store');

  Route.post('/forms/admin-delete','FormOwnersController.destroy');

  Route.get('/forms/checkslug/:slug','FormsController.checkSlug');

 

}).middleware('auth').prefix('/api')

Route.post("/tripay/callback","PaymentsController.tripayCallback")

Route.post("/moota/callback","PaymentsController.mootaCallback")



 
// vue template came here
Route.get('*', async ({ view,auth }) => {   
  try {
    
    await auth.use('web').authenticate()

    if(auth.user)
    {
      let user = auth.user;
    
      user.password = '';

      return view.render('welcome',{user})
    } 

  } catch {
    return view.render('welcome')
  }   
})



