require('dotenv').config()  



const redis = require("redis");
 
const client = redis.createClient ({
    prefix: 'dripform:'
});
  


   
import Scylla from "./app/Services/Scylla"


const a = Date.now();
Scylla.boot(()=>{

    let tables = ["whatsapp-device","whatsapp-followup","email-followup","moota-config","tripay-config","tripay-reference","bank-transaction"];
    let models = ["WhatsappDevice","WhatsappFollowup","EmailFollowup","BankToken","Tripay","TripayTransaction","BankTransaction"];
    let pointer = 0;


    
    async function LoopFunction(){

    console.log(models[pointer])

    Scylla.models.instance[models[pointer]].eachRow({}, {fetchSize : 50, raw : true}, function(_n, row){
        // invoked per each row in all the pages
        if(row.id)
        row.id = row.id.toString() 

        if(["whatsapp-device","whatsapp-followup","email-followup"].includes(tables[pointer]))
        {
            client.hset(tables[pointer]+":"+row.form_id,row.id,JSON.stringify(row));
        } 
        else if("moota-config" == tables[pointer]){
            client.set("moota-config:"+row.user_id,JSON.stringify(row))
        }else if("tripay-config" == tables[pointer]){
            client.set("tripay-config:"+row.user_id,JSON.stringify(row))
        }else if("tripay-reference" == tables[pointer])
        {
            client.setex("tripay-reference:"+row.reference,86400,JSON.stringify(row));
        }else if("bank-transaction" == tables[pointer])
        {
            client.setex("bank-transaction:"+row.bank_account+row.price,86400,JSON.stringify(row));
        }
       
    }, async function(err, result){
        // called once the page has been retrieved.
        if(err) throw err;

       
       
        
        if (result.nextPage) {
            // console.log(result)
            // retrieve the following pages
            // the same row handler from above will be used
           
      
            result.nextPage();

        }else{
            console.log("selesai insert table "+tables[pointer])

            pointer++;
            
            if(pointer < tables.length)
            {
                LoopFunction()
            }else{
                const b = Date.now();

                console.log('selesai dalam '+((b - a)/1000)+' s')
                process.exit(1)
            }
             
        } 
        
        // let forms = await knex.select().table('forms');
        // console.log(forms)
    });

    }

    LoopFunction();



});



// 1. copy forms table ke cockroach


// 2. copy users table ke cockroach


// 3. copy leads table ke cockroach

// 4. copy formsowners table ke cockroach

// 5. copy whatsapp device ke redis



// 6. copy whatsapp followup ke redis

// Redis.hset("whatsapp-device:"+request.input("form_id"),request.input('id'),JSON.stringify(request.all()));

// 7. copy email followup ke redis

// 8. copy locked content ke redis

// 9. copy tripay ke redis

// 10. copy bank token ke redis

// 11. copy tripay transaction ke redis

// 12. copy bank transaction ke redis

// 