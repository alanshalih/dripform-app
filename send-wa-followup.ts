 
import axios from "axios";
import Scylla from "./app/Services/Scylla"

Scylla.boot(function(){
    var query = {
        send_at:{
            '$token':{'$lt': Date.now().toString()}
        }
    };
    Scylla.models.instance.WhatsappFollowupQueue.find(query,function(err,data){
        if(err)
        {
            throw err;
        } 
      
        if(data.length)
        {

            data.forEach((item,index)=>{
             
                Scylla.models.instance.Lead.findOne({id : item.lead_id},{raw : true,select : ["id","status"]},function(err,lead){
                    if(err) throw err;
    
                    if(lead.status == item.status)
                    {
                        
                        
                        let query = {text : item.content, phone : item.lead_phone, api_key : item.whatsapp_id} as any; 

                        if(item.hasButton)
                        {
                            query.type  = "buttonsMessage"
                            query.footerText = item.footerText;
                            query.buttons = item.buttons.map((item,index)=>{
                                return {buttonId : 'id'+index, buttonText : {displayText : item}, type : 1}
                            })
                        } 

                        axios.post("http://api.dripsender.id/send",query).then(()=>{

                            // hapus pesan setelah dikirim
                            console.log('pesan dikirim')
                            item.delete(function(err){
                                if(err) throw err;
        
                                if(index == data.length-1)
                                {
                                    setTimeout(()=>{
                                        process.exit(0)
                                    },3000)
                                } 
                            })  

                        },err=>{
                            console.log(err)

                            
                            // item.tries = item.tries ? item.tries++ : 1;
                            
                            // if(item.tries < 4)
                            // {
                            //     item.save(function(err){
                            //         if(err) throw err;
            
                            //         if(index == data.length-1)
                            //         {
                            //             setTimeout(()=>{
                            //                 process.exit(0)
                            //             },3000)
                            //         } 
                            //     });
                            // }else{
                               
                            // }

                            item.delete(function(err){
                                if(err) throw err;
        
                                if(index == data.length-1)
                                {
                                    setTimeout(()=>{
                                        process.exit(0)
                                    },3000)
                                } 
                            }) 


                            

                        })
                    }else{
                        // status ga sama
                        // hapus pesan
                        item.delete(function(err){
                            if(err) throw err;
    
                            if(index == data.length-1)
                            {
                                setTimeout(()=>{
                                    process.exit(0)
                                },3000)
                            } 
                        })  

                    }
    
                    
     
                })
            })
            
        }else{
            process.exit(0)
        }
        
    })

});

