require('dotenv').config() 
 
import Scylla from "./app/Services/Scylla"

Scylla.boot(()=>{});

const crypto = require('crypto')

const axios = require('axios'); // using Axios library 

const fastify = require('fastify')({
    logger: {
        level: 'error'
    }
})


import Event from './formevent'



fastify.register(require('fastify-cors'), { 
    // put your options here
  })
  

fastify.register(require('point-of-view'), {
    engine: {
        ejs: require('art-template')
    }
})

const version = "@1.0.43"



const redis = require("redis");
const client = redis.createClient();

client.on("error", function (err) {
    console.log("Error " + err);
});

const {
    promisify
} = require("util"); 

const get_ttl =   promisify(client.ttl).bind(client);
  

const getAsync =   promisify(client.get).bind(client);

const get_incrby =   promisify(client.INCRBY).bind(client);
 
 

const path = require('path')
fastify.register(require('fastify-static'), {
    root: path.join(__dirname, 'public/assets'),
    prefix: '/public/assets/', // optional: default '/'
})

// Declare a route
fastify.get('/', async (request,reply) => {
    
    
    let domain = await Scylla.models.instance.Domain.findOneAsync({name : request.hostname});

    if(domain)
    {
        if(domain.landing_page)
        {
            return reply.redirect(domain.landing_page)
        }else{
            return reply.redirect("https://drip.id");
        }
    }else{
        return reply.redirect("https://drip.id");
    }
    
})


fastify.get('/check-api', async () => {
    return 'ok'
})



fastify.get('/:id', async (request, reply) => {

    try {

        let data = await Scylla.models.instance.Form.findOneAsync({
            slug: request.params.id
        }, {
            raw: true
        })

     



        if (!data) {
            return 'form tidak ditemukan';
        }

        if(data.domain != request.hostname)
        {
            return 'domain form salah';
        }

        if(data)
        {
            data.hit_counter =  data.hit_counter ? (data.hit_counter + 1) : 1
        
            Scylla.models.instance.Form.update({
                id: data.id
            }, {
                hit_counter: data.hit_counter,
                updated : Date.now().toString()
            });
        }
       

        if (typeof data == 'object') {
            data.list_field = JSON.parse(data.list_field)
            data.response_fields = JSON.parse(data.response_fields)
        }

        let head_script = data.head_script || '';
        let body_script = data.body_script || '';

        delete data.body_script;
        delete data.head_script;

        return reply.view('/resources/views/layout.art', {
            form: data,
            head_script,
            body_script,
            version,
            form_string: JSON.stringify(data)
        })

    } catch (err) {
        return err;
    }


})


fastify.get('/locked-content/:id', async (request) => {

    try { 
        let data = await Scylla.models.instance.LockedContent.findOneAsync({
            id: Scylla.models.uuidFromString(request.params.id)
        }, {
            raw: true
        }) 
        

        if(data && request.query.lead_id)
        {
      
            let lead = await Scylla.models.instance.Lead.findOneAsync({
                id: request.query.lead_id
            }, {
                raw: true
            }) 
           
        
            
            if(data.status.includes(lead.status) && lead.point >= data.point)
            {
                return data;
            }
            return;
        }else{
            return;
        } 

    } catch (err) {
        return err;
    }


})


 



fastify.post('/form', async (request) => {
    try {
 
        let rc = request.headers.cookie;

        let data = request.body;

        if (!data.form_id) {
            return;
        }


        data.id = Math.random().toString(36).slice(2);

        data.all_columns = JSON.stringify(data);

        data.created = Date.now().toString()

        let lead;
 

        if (data.unique_identifier) {
 

            let lead;
            
            if(data.unique_identifier == 'phone')
            {
                lead = await Scylla.models.instance.Lead.findOneAsync({form_id : data.form_id, phone : data.phone}, {
                    materialized_view: 'lead_by_phone', 
                }) 
            }else{
                lead = await Scylla.models.instance.Lead.findOneAsync({form_id : data.form_id, email : data.email}, {
                    materialized_view: 'lead_by_email', 
                }) 
            }
           


            if (lead) {
                lead = await Scylla.models.instance.Lead.findOneAsync({id : lead.id})
                return lead;
            } else {

                lead = new Scylla.models.instance.Lead(data)

                lead.updated = Date.now().toString();

                if(rc)
                {
                    let hasAffiliate = rc.split(';').find(item => {
                        return item.includes(data.form_id + '-affiliate');
                    })


                    
                    if (hasAffiliate) {
                        const upline_id = hasAffiliate.split('=')[1]

                        lead.upline_id = upline_id;
                        
                    }
                }


 

                await lead.saveAsync();

                client.incr("leads:" + data.form_id, (err, lead_number) => {

                    if(err)
                    {
                        console.error(err);
                    }
                    if (lead_number)
                    { 
                        Scylla.models.instance.Form.findOne({
                            id: Scylla.models.uuidFromString(data.form_id)
                        },function(err,form){
                            if(err) throw err; 
                            
                            if(form)
                            {
                                if(form.wa_automation)
                                {
                                    Event.emit('trigger:wa_automation', data)
                                }
                                
                                if(form.email_automation)
                                {
                                    Event.emit('trigger:email_automation', data)
                                }
                                if(form.webhook_automation)
                                {
                                    Event.emit('trigger:webhook_automation', data)
                                }
                                if(form.obs_automation)
                                {
                                    Event.emit('trigger:obs_automation', data)
                                }
                                
    
                                form.lead_number = parseInt(lead_number);
                                form.updated = Date.now().toString();
                                form.save();  

                                if(rc)
                                {
                                    let exist = rc.split(';').find(item => {
                                        return item.includes(data.form_id + '-affiliate');
                                    })
 

                                    
                                    if (exist) {
                                        const affiliate_id = exist.split('=')[1]


 

                                        client.INCRBY("point:" + affiliate_id.trim(), form.lead_point, (err, point) => {
                                            if(err)
                                            {
                                                console.log(err)
                                            }
                                            if (point)
                                            {

                                                client.INCR("lead_number:" + affiliate_id.trim(), (err, lead_number) => {
                                                    if(err)
                                                    {
                                                        console.log(err)
                                                    }
                                                    if (lead_number)
                                                    {
                                                        Scylla.models.instance.Lead.update({
                                                            id: affiliate_id.trim(),
                                                            form_id: data.form_id,
                                                        }, {
                                                            point: parseInt(point),
                                                            lead_number : parseInt(lead_number)
                                                        });
                                                    }
                                                    
                                                })

                                                Scylla.models.instance.Lead.update({
                                                    id: affiliate_id.trim(),
                                                    form_id: data.form_id
                                                }, {
                                                    point: parseInt(point)
                                                });
                                            }
                                            
                                        })

                                    }
                                }
                            }
                        });

                    }
                         
                }) 
                
                
                
                

                return data
            }







        } else {
            lead = new Scylla.models.instance.Lead(data)

            lead.updated = Date.now().toString();

 

            await lead.saveAsync()

            client.incr("leads:" + data.form_id, (err, lead_number) => {
                
                console.log(err)

                if (lead_number)
                    Scylla.models.instance.Form.findOne({
                        id: Scylla.models.uuidFromString(data.form_id)
                    },function(err,form){
                        if(err) throw err; 

                        if(form)
                        {
                            if(form.wa_automation)
                            {
                                Event.emit('trigger:wa_automation', data)
                            }
                            
                            if(form.email_automation)
                            {
                                Event.emit('trigger:email_automation', data)
                            }
                            if(form.webhook_automation)
                            {
                                Event.emit('trigger:webhook_automation', data)
                            }
                            if(form.obs_automation)
                            {
                                Event.emit('trigger:obs_automation', data)
                            }
                            

                            form.lead_number = parseInt(lead_number);
                            form.updated = Date.now().toString();
                            form.save();
                        }
                    });

                    
            }) 

            return data;
        }

    } catch (err) {
        return err;
    }



});

function randomIntFromInterval(min, max) { // min and max included
    return Math.floor(Math.random() * (max - min + 1) + min)
  }

fastify.post("/payment/bank", async (request, reply) => {
    try {
        let form = await Scylla.models.instance.Form.findOneAsync({id : Scylla.models.uuidFromString(request.body.form_id)});

        let lead = await Scylla.models.instance.Lead.findOneAsync({id : request.body.lead_id});

        let token = await Scylla.models.instance.BankToken.findOneAsync({user_id : Scylla.models.uuidFromString(form.user_id)});

        let token_id;

        if(token)
        {
            token_id = token.token;
        }
        let channel = request.body.payment_method;

        let data = JSON.parse(lead.all_columns); 

        let transaction = {price : data.total, bank_account : channel.bank_account, lead_id : lead.id, token_id, unique_code : 0  };
        
        const loopPrice =  async function(){
            if(channel.unique_code_modifier == 'none')
                {
                    channel.expired_time = Date.now()+86400; 
                    lead.payment_data = JSON.stringify(channel);
                    lead.payment_method = channel.bank_name;
                    lead.payment_gateway = "bank";
                    lead.save(); 
                    lead.all_columns = data;
                    lead.payment_data = channel; 

                    return reply.send(lead);
                }

                if(channel.unique_code_modifier == 'up')
                {
                    transaction.unique_code = randomIntFromInterval(1,999)
                    transaction.price = data.total + transaction.unique_code;
                }

                if(channel.unique_code_modifier == 'down')
                {
                    transaction.unique_code = randomIntFromInterval(1,999)
                    transaction.price = data.total - transaction.unique_code;
                }
                 
                    let check = await Scylla.models.instance.BankTransaction.findOneAsync({price : transaction.price, bank_account : transaction.bank_account});

                    if(check)
                    { 
                        loopPrice();
                    
                    }else{ 

                        var price = new Scylla.models.instance.BankTransaction(transaction);
                        // save for 24 hours
                        price.save({ttl: 86400}, async function(err){
                            if(err) throw err; 
                           
                        });
 

                          data.unique_code = transaction.unique_code; 
                          data.unique_price = transaction.price;
                          channel.expired_at = Date.now()+86400;
                          
                 
                          lead.all_columns = JSON.stringify(data);
                          lead.payment_data = JSON.stringify(channel);
                          lead.payment_method = channel.bank_name;
                          lead.payment_gateway = "bank";
                          lead.save(); 
                          lead.all_columns = data;
                          lead.payment_data = channel; 

                          return reply.send(lead);

                    }
            
        } 

            loopPrice();
      
        



    } catch (error) {
        return error;
    }
        
});

fastify.post("/payment/tripay", async (request) => {
    
    try {
        let form = await Scylla.models.instance.Form.findOneAsync({id : Scylla.models.uuidFromString(request.body.form_id)});

    let lead = await Scylla.models.instance.Lead.findOneAsync({id : request.body.lead_id});

    let data = JSON.parse(lead.all_columns);

    let tripay = await Scylla.models.instance.Tripay.findOneAsync({user_id : Scylla.models.uuidFromString(form.user_id)});



    var apiKey = tripay.apiKey;

    var privateKey = tripay.privateKey;
    
    var merchant_code = tripay.merchant_code;

    var merchant_ref = lead.id;

    var amount = data.total;

    data.products = data.products.filter(item=>{
        item.name = item.title ? item.title : item.name ;
        return item.quantity;
    })
 

    if(data.courier_cost)
    {
        data.products.push({
            "sku": "ONGKIR",
            "name": `Ongkos kirim menggunakan ${data.courier_name} - ${data.courier_service}`,
            "price": data.courier_cost,
            "quantity": 1,
            "subtotal": data.courier_cost
        })
    }
 
 
 
     
    
    var signature = crypto.createHmac('sha256', privateKey).update(merchant_code + merchant_ref + amount ).digest('hex'); 
    
    var payload = {
        'method': request.body.payment_method,
        'merchant_ref': merchant_ref,
        'amount': amount,
        'customer_name': lead.name,
        'customer_email': lead.email ? lead.email : 'customer@drip.id',
        'customer_phone': lead.phone,
        'order_items': data.products,
        'callback_url': tripay.urlCallback,
        'return_url': 'https://'+form.domain+'/r/'+lead.id, 
        'signature': signature
      }
     
     let response =  await axios.post(process.env.TRIPAY_URL, payload, {
      headers: {
        'Authorization': 'Bearer ' + apiKey
      }
    })
 
    if(response)
    {
        lead.payment_data = JSON.stringify(response.data);
        lead.payment_method = request.body.payment_method;
        lead.payment_gateway = "tripay";
        lead.save();
        lead.payment_data = response.data;

        let trx = new Scylla.models.instance.TripayTransaction({reference : response.data.data.reference, lead_id : lead.id, privateKey : tripay.privateKey });
        
        trx.save({ttl: 86400});


        return lead;


    }
    } catch (error) {
        console.log(error)
            return error;
    }
 
 
});



fastify.get('/r/:id', async (request, reply) => {

    try {

        let lead = await Scylla.models.instance.Lead.findOneAsync({
            id: request.params.id
        }, {
            raw: true
        })



        if (!lead) {

            return 'data tidak ditemukan';
        } else {
            lead.all_columns = JSON.parse(lead.all_columns);
            lead.payment_data = lead.payment_data ? JSON.parse(lead.payment_data) : {};
        }

        let form = await Scylla.models.instance.Form.findOneAsync({
            id: Scylla.models.uuidFromString(lead.form_id)
        }, {
            raw: true
        })


        if (form) {
            form.list_field = JSON.parse(form.list_field)

            form.response_fields = JSON.parse(form.response_fields)
        } else {
            return 'form tidak ditemukan'
        }

        let head_script = form.head_script || '';
        let body_script = form.body_script || '';

        delete form.body_script;
        delete form.head_script;

      

        return reply.view('/resources/views/layout.art', {
            form: form,
            form_string: JSON.stringify(form),
            lead: lead,
            head_script,
            body_script,
            version,
            lead_string: JSON.stringify(lead)
        })

    } catch (err) {
        return err;
    }

})



fastify.get('/s/:id', async (request, reply) => {

    try {

        let lead = await Scylla.models.instance.Lead.findOneAsync({
            id: request.params.id
        }, {
            raw: true
        })




        if (!lead) {
            return 'data tidak ditemukan';
        }

        let form = await Scylla.models.instance.Form.findOneAsync({
            id: Scylla.models.uuidFromString(lead.form_id)
        }, {
            raw: true
        })

        if (form) {

            reply.raw.writeHead(302, {
                'Set-Cookie': form.id + '-affiliate=' + lead.id + ";Path=/",
                'location': 'http://' + form.domain + '/' + form.slug
            });
            reply.raw.end();
            reply.sent = true;
            return;

        }
    } catch (err) {

        return err;
    }

})


fastify.get('/form/:id/leaderboard', async (request) => {

    let leader = await getAsync("leaderboard:"+request.params.id);

    if(leader)
    {
        return leader;
    }else{

    const limit = parseInt(request.query.limit) || 10;

    let data = await Scylla.models.instance.Lead.findAsync({form_id: {'$eq' : request.params.id},   $orderby: { '$desc' :'point' }, $limit : limit}, {select : ["name","point"], materialized_view: 'leaderboard', raw: true});

    client.setex("leaderboard:"+request.params.id,3600,JSON.stringify(data))
    return data;

    }
      
    

})

fastify.get('/link/:id', async (request,params) => {
      
 
    
    const urls = request.query['urls[]']; 

    let index = await get_incrby("selected_link:"+params.id ,1);

    
    if(index >= urls.length)
    {
        index = 1;
        client.set("selected_link:"+params.id, 0);
    }
 
    return urls[index-1];

})





fastify.get('/api/lead/:id', async (request) => {


    try {

        let lead = await Scylla.models.instance.Lead.findOneAsync({
            id: request.params.id
        }, {
            raw: true
        })
        
        lead.all_columns = JSON.parse(lead.all_columns) 
        lead.payment_data = lead.payment_data ? JSON.parse(lead.payment_data) : {};
        return lead;

    } catch (err) {
        return err;
    }


})




fastify.post('/share1', async (request) => {



    let data = request.body;

    if (!data.lead_id) {
        return 'lead_id tidak ada';
    }

    try {

        
        const ttl = await get_ttl('action_id:' + data.action_id + ':lead_id:' + data.lead_id)

        if (ttl > 0) {
            console.log('action ID in cooldown')
            return {
                status: 'cooldown',
                ttl: ttl
            };
        } else {

            let form = await Scylla.models.instance.Form.findOneAsync({
                id: Scylla.models.uuidFromString(data.form_id)
            }); 

            if (form) {
                const response_fields = JSON.parse(form.response_fields)

                let findAction = response_fields.find(item => {
                    return item.action_id == data.action_id;
                })

                if (findAction) {

                    // // set action cooldown
                    client.setex('action_id:' + data.action_id + ':lead_id:' + data.lead_id, findAction.action_cooldown * 60, 'exist');

                    
                    let point = await get_incrby("point:" + data.lead_id,findAction.point);
                    

               
                    if(point)
                    {
                        Scylla.models.instance.Lead.update({
                            id: data.lead_id.trim(),
                            form_id : form.id.toString()
                        }, {
                            point: parseInt(point)
                        });

                        return {
                            status: 'success',
                            point,
                            ttl: findAction.action_cooldown * 60
                        }
                    }

  



                } else {
                    return;
                }


            } else {

                return 'form tidak ditemukan';
            }

        }

    } catch (error) {
        return error;
    }


 



});

 

// Run the server!
const start = async () => {
    try {
        await fastify.listen(4444,'0.0.0.0')
    } catch (err) {
        fastify.log.error(err)
        process.exit(1)
    }
}
start()


// event

import dayjs from 'dayjs'

 

Event.on('trigger:wa_automation', (lead) => {
 

    const all_columns = JSON.parse(lead.all_columns)

    Scylla.models.instance.WhatsappDevice.find({form_id : lead.form_id},{raw : true},function(err,devices){

        if(err) throw err; 

        if(devices)
        {
            if(devices.length)
            {

                const device = devices[Math.floor(Math.random() * devices.length)];

                Scylla.models.instance.Lead.update({
                    id: lead.id,
                    form_id : lead.form_id
                }, {
                    whatsapp_device_id : device.id.toString()
                },function(err){
                    if(err) throw err; 
                });



                Scylla.models.instance.WhatsappFollowup.find({form_id : lead.form_id},{raw : true},function(err,rules){
                    if(err) throw err; 
                    if(rules)
                    {
                          rules.forEach(item=>{
                            if(item.status == lead.status)
                            {
                                let data = item;

                                data.id = Scylla.models.uuid();
            
 

                                Object.keys(all_columns).forEach(key => {
                                    if (key)
                                    {
                                        if(['donasi','total','subtotal','courier_cost'].includes(key))
                                        {
                                            data.content = data.content.split('[' + key + ']').join(parseInt(all_columns[key]).toLocaleString('id'))
                                        }else{
                                            data.content = data.content.split('[' + key + ']').join(all_columns[key])
                                        }
                                        
                                    }
                                        
                                })

                                
            
                                data.whatsapp_id = device.whatsapp_id

                                data.whatsapp_device_id = device.id.toString();

                                data.sender_name = device.name;

                                data.send_at = dayjs().add(item.send_after, item.send_after_unit).valueOf().toString(); 

                                data.lead_id = lead.id;

                                data.lead_name = lead.name;

                                data.lead_phone = lead.phone;
            
                                let queue = new Scylla.models.instance.WhatsappFollowupQueue(data);
            
                                queue.save(function(err){
                                    if(err) throw err; 
                                });
                            }
                        }) 
                    }
                })

            }
        }

        

    })

    
 })
 Event.on('trigger:email_automation', (lead) => {

    const all_columns = JSON.parse(lead.all_columns)

    if(lead.email)
    {

        Scylla.models.instance.EmailFollowup.find({form_id : lead.form_id},{raw : true},function(err,rules){
            if(err) throw err; 
            if(rules)
            {
                  rules.forEach(item=>{
                    if(item.status == lead.status)
                    {
                        let data = item;
    
                        data.id = Scylla.models.uuid();
    
    
    
                        Object.keys(all_columns).forEach(key => {
                            if (key)
                            {
                                if(['donasi','total','subtotal','courier_cost'].includes(key))
                                {
                                    data.content = data.content.split('[' + key + ']').join(parseInt(all_columns[key]).toLocaleString('id'))
                                    data.subject = data.subject.split('[' + key + ']').join(parseInt(all_columns[key]).toLocaleString('id'))
                                }else{
                                    data.content = data.content.split('[' + key + ']').join(all_columns[key])
                                    data.subject = data.subject.split('[' + key + ']').join(all_columns[key])
                                }
                                
                            }
                                
                        })
    
                        
     
    
                        data.sender_name = item.sender_name;
    
                        data.send_at = dayjs().add(item.send_after, item.send_after_unit).valueOf().toString(); 
    
                        data.lead_id = lead.id;
    
                        data.lead_name = lead.name;
    
                        data.lead_email = lead.email;
    
                        let queue = new Scylla.models.instance.EmailFollowupQueue(data);
    
                        queue.save(function(err){
                            if(err) throw err; 
                        });
                    }
                }) 
            }
        })

    }
    

})
Event.on('trigger:webhook_automation', (lead) => {
    console.log(lead)
})
Event.on('trigger:obs_automation', (lead) => {
    console.log(lead)
})