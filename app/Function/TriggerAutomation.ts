import Scylla from 'App/Services/Scylla'; 
const dayjs = require('dayjs')

export function TriggerWAAutomation(lead, all_columns){
    Scylla.models.instance.WhatsappDevice.findOne({form_id : lead.form_id, id : Scylla.models.uuidFromString(lead.whatsapp_device_id)},{raw : true},function(err,device){
        if(err) throw err; 

        if(device)
        {
            Scylla.models.instance.WhatsappFollowup.find({form_id : lead.form_id},{raw : true},function(err,rules){
                if(err) throw err; 
                if(rules)
                {
                      rules.forEach(item=>{
                        if(item.status == lead.status && lead.whatsapp_device_id)
                        {
                            let data = item;

                            data.id = Scylla.models.uuid(); 
                            
                            Object.keys(all_columns).forEach(key => {
                                if (key)
                                {
                                    if(['donasi','total','subtotal','courier_cost'].includes(key))
                                    {
                                        data.content = data.content.split('[' + key + ']').join(parseInt(all_columns[key]).toLocaleString('id'))
                                    }else{
                                        data.content = data.content.split('[' + key + ']').join(all_columns[key])
                                    }
                                    
                                }
                                    
                            })

                            
        
                            data.whatsapp_id = device.whatsapp_id

                            data.whatsapp_device_id = device.id.toString();

                            data.sender_name = device.name;

                            data.send_at = dayjs().add(item.send_after, item.send_after_unit).valueOf().toString(); 

                            data.lead_id = lead.id;

                            data.lead_name = lead.name;

                            data.lead_phone = lead.phone;
        
                            let queue = new Scylla.models.instance.WhatsappFollowupQueue(data);
        
                            queue.save(function(err){
                                if(err) throw err; 
                            });
                        }
                    }) 
                }
            })

        }
       });
}
export function TriggerEmailAutomation(lead, all_columns){
    if(lead.email)
    {

        Scylla.models.instance.EmailFollowup.find({form_id : lead.form_id},{raw : true},function(err,rules){
            if(err) throw err; 
            if(rules)
            {
                  rules.forEach(item=>{
                    if(item.status == lead.status)
                    {
                        let data = item;
    
                        data.id = Scylla.models.uuid();
    
    
    
                        Object.keys(all_columns).forEach(key => {
                            if (key)
                            {
                                if(['donasi','total','subtotal','courier_cost'].includes(key))
                                {
                                    data.content = data.content.split('[' + key + ']').join(parseInt(all_columns[key]).toLocaleString('id'))
                                    data.subject = data.subject.split('[' + key + ']').join(parseInt(all_columns[key]).toLocaleString('id'))
                                }else{
                                    data.content = data.content.split('[' + key + ']').join(all_columns[key])
                                    data.subject = data.subject.split('[' + key + ']').join(all_columns[key])
                                }
                                
                            }
                                
                        })
    
                        
     
    
                        data.sender_name = item.sender_name;
    
                        data.send_at = dayjs().add(item.send_after, item.send_after_unit).valueOf().toString(); 
    
                        data.lead_id = lead.id;
    
                        data.lead_name = lead.name;
    
                        data.lead_email = lead.email;
    
                        let queue = new Scylla.models.instance.EmailFollowupQueue(data);
    
                        queue.save(function(err){
                            if(err) throw err; 
                        });
                    }
                }) 
            }
        })

    }
}