

module.exports = {
    fields:{  
        id : 'uuid',
        form_id : 'varchar', 
        content : "text",
        subject : 'varchar',
        title : 'varchar',
        send_at : 'varchar',
        status : 'varchar', 
        sender_name : 'varchar',
        sender_email : 'varchar',
        lead_id : 'varchar',
        lead_name : 'varchar',
        lead_email : 'varchar',
        tries : {
            type : 'int',
            default : 0
        }
        
    },
    key : [["send_at"],'id'],
    indexes : ["form_id"]
}
 