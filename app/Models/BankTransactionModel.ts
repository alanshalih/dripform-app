
module.exports = {
    fields:{
        price : "int",
        bank_account  : "varchar",
        lead_id  : "varchar",
        token_id  : "varchar"
    },
    key:[["price","bank_account"]]
}
