
module.exports = {
    fields:{
        id : {
            type: "uuid",
            default: {"$db_function": "uuid()"}
        }, 
        form_id : 'varchar', 
        type : "varchar",
        bank_name : "varchar",
        account_name : "varchar",
        account_number : "varchar"
    },
    key : [["form_id","id"]]
}
