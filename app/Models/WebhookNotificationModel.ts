

module.exports = {
    fields:{ 
        id : 'uuid',
        form_id : 'varchar', 
        url : "text",
        method : "varchar",
        title : 'varchar',
        send_after : 'varchar',
        send_after_unit : 'varchar', 
        status : 'varchar',
        schema : 'text'
    },
    key : [["form_id"],"id"]
}
 