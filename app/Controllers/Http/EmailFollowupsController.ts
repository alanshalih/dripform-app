import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Scylla from 'App/Services/Scylla';

export default class EmailFollowupsController {

  public async index ({request}: HttpContextContract) {
    return Scylla.models.instance.EmailFollowup.findAsync({form_id : request.input("form_id")})
  }

  public async create ({}: HttpContextContract) {
  }

  public async store ({request}: HttpContextContract) {

      let reqData = request.all();

      reqData.id = Scylla.models.uuid();
      
      let data = new Scylla.models.instance.EmailFollowup(reqData);
      
      data.save();

      return reqData;

  }

  public async show ({}: HttpContextContract) {
  }

  public async edit ({}: HttpContextContract) {
  }

  public async update ({request}: HttpContextContract) {
    Scylla.models.instance.EmailFollowup.update({id : Scylla.models.uuidFromString(request.input('id')), form_id : request.input("form_id")},request.except(['id',"form_id"]))
  }

  public async destroy ({params}: HttpContextContract) {
    Scylla.models.instance.EmailFollowup.findOne({id: Scylla.models.uuidFromString(params.id)}, function(err, data){
      if(err) throw err;
  
      //Note that returned variable john here is an instance of your model,
      //so you can do john.delete() like the following
      data.delete(function(err){
          //...
          if(err) throw err;
      });
  });

  }
}
