import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Scylla from 'App/Services/Scylla';
import axios from 'axios';
 

export default class DomainsController {
  public async index ({auth}: HttpContextContract) {

    if(!auth.user)
    {
      return;
    }

    let domains = await Scylla.models.instance.Domain.findAsync({main_domain : true},{raw : true});

    if(domains.length == 0)
    {
      domains.push({
        name : process.env.FORM_DOMAIN,
        main_domain : true
      })
    }


    const userDomain = await Scylla.models.instance.Domain.findAsync({user_id : auth.user.id.toString()},{raw : true});

    domains = domains.concat(userDomain);

    return domains;

  }

  public async create ({}: HttpContextContract) {
  }

  public async store ({request,response,auth}: HttpContextContract) {

    try {

      if(!auth.user)
      {
        return response.status(401).send("Tidak terauthentikasi");
      }

      let domainData =  await axios.get('https://'+request.input('domain')+'/check-api',{timeout : 1000})
     
      if(domainData.headers.server == 'cloudflare')
      {
        let data = {
          id : Scylla.models.uuid(),
          name : request.input("domain"),
          user_id : auth.user.id.toString(),
          main_domain : false
        }

        let domain = new Scylla.models.instance.Domain(data);

        domain.save();

        return response.send(data); 
      }else{
        return response.status(401).send("Domain tidak terhubung")
      }


    } catch (error) {

  

      return response.status(401).send('Domain tidak terhubung')
    
    }
   

   
  }

  public async show ({}: HttpContextContract) {
  }

  public async edit ({}: HttpContextContract) {
  }

  public async update ({}: HttpContextContract) {
  }

  public async destroy ({params}: HttpContextContract) {

    Scylla.models.instance.Domain.findOne({id: Scylla.models.uuidFromString(params.id)}, function(err, data){
      if(err) throw err;
  
      //Note that returned variable john here is an instance of your model,
      //so you can do john.delete() like the following
      if(data)
      data.delete(function(err){
          //...
          if(err) throw err;
      });
  });

  }

  public async mainDomain(){

    let domain = await Scylla.models.instance.Domain.findOneAsync({main_domain : true});

    if(domain)
    return domain.name;
    else return process.env.FORM_DOMAIN;
  
  }

  public async saveLandingPage ({params,auth,response,request}: HttpContextContract) {
 
    if(!auth.user)
    {
      return response.status(404).send("tidak terauthentikasi")
    }

    try {
   
      let domain = await   Scylla.models.instance.Domain.findOneAsync({id : Scylla.models.uuidFromString(params.id)});

       

        domain.landing_page = request.input("landing_page");

        domain.save();

      
    } catch (error) {
      console.log(error)
      return response.status(404).send(error)
    }


  }

}
