import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Scylla from 'App/Services/Scylla';

export default class LockedContentsController {
  public async index ({}: HttpContextContract) {
  }

  public async create ({}: HttpContextContract) {
  }

  public async store ({request}: HttpContextContract) {
    let data = request.all();
    data.id = Scylla.models.uuid();
    data.point = parseInt(data.point)
    let result = new Scylla.models.instance.LockedContent(data);
    result.save();
    return data;
  }

  public async show ({params}: HttpContextContract) {
    return await Scylla.models.instance.LockedContent.findOneAsync({id : Scylla.models.uuidFromString(params.id)});
  }

  public async edit ({}: HttpContextContract) {
  }

  public async update ({params,request}: HttpContextContract) {
    let data = request.except(["id"]);
    data.point = parseInt(data.point)
    Scylla.models.instance.LockedContent.update({id : Scylla.models.uuidFromString(params.id)},data);
  }

  public async destroy ({params}: HttpContextContract) {
    Scylla.models.instance.LockedContent.delete({id : Scylla.models.uuidFromString(params.id)});
  }
}
