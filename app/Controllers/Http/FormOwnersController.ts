import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Scylla from 'App/Services/Scylla';

export default class FormOwnersController {
  public async index ({params}: HttpContextContract) {
    const admins = await Scylla.models.instance.FormOwner.findAsync({form_id : params.form_id},{ raw : true }); 

    const usersIds = admins.map(item=>Scylla.models.uuidFromString(item.user_id));

     const users = await Scylla.models.instance.User.findAsync({id :  {'$in' : usersIds}},{ raw : true, select : ["name","id","email"] }); 

    return {admins, users};
  }

  public async create ({}: HttpContextContract) {
  }

  public async store ({params,request,response}: HttpContextContract) {

    const user = await Scylla.models.instance.User.findOneAsync({email : request.input("email")});

    if(!user)
    {
      return response.status(404).send('User Not Found') 
    }
    let formOwner = new Scylla.models.instance.FormOwner({form_id : params.form_id, user_id : user.id.toString(), role : "admin"})
    
    await formOwner.saveAsync({if_not_exist : true})

    return 'ok'

  }

  public async show ({}: HttpContextContract) {
  }

  public async edit ({}: HttpContextContract) {
  }

  public async update ({}: HttpContextContract) {
  }

  public async destroy ({request,response}: HttpContextContract) {

    console.log(request.all())
    Scylla.models.instance.FormOwner.delete({form_id : request.input("form_id"), user_id : request.input("user_id")}, function(err){
      if(err) {
        return response.status(500).send('Unknown Error') 
      }
      else {
        return response.send("ok")
      };
  });

 

  
  }
}
