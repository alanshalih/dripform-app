 
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Scylla from 'App/Services/Scylla'

export default class FormsController {
  
  public async index ({auth,response}: HttpContextContract) {

    if(!auth.user)
    {
      return response.abort("Tidak terauthentikasi")
    }

    const listForms = await Scylla.models.instance.FormOwner.findAsync({user_id : auth.user.id.toString()},{select: ['form_id'] }); 

    if(listForms.length == 0)
    { 
      return []
    }
  
    const forms = listForms.map(item=>{
      return Scylla.models.uuidFromString(item.form_id);
    });
 
    let result = [] as any;
 

    // for await (const item of listForms) {
    //   const data = await Scylla.models.instance.Form.findOneAsync({id : Scylla.models.uuidFromString(item.form_id)});
    //   result.push(data)
    // }
  
  


   let promise = new Promise((resolve, reject) => {
      Scylla.models.instance.Form.eachRow({id : {'$in' : forms} }, {raw: true}, function(_n, row){
       if(row)
        result.push(row)
     }, function(err){
       if(err){
         reject(err);
        throw err;
       }; 
       resolve(result)
     });
    });

    return await promise;
  
      

  // new Promise((resolve, reject) => {
  //     Scylla.models.instance.Form.stream({id : {'$in' : forms} }, {raw: true}, function(reader){
  //       var row;
  //       while (row = reader.readRow()) {
  //           //process row
  //           result.push(row)
  //       }
  //   }, function(err){
  //     if(err){
  //              reject(err);
  //             throw err;
  //            }; 
  //     resolve(result)
  //       //emitted when all rows have been retrieved and read
  //   });
  // });

  return result;



    // let query = {id : {'$in' : forms} } as any;

    // console.log(query)

    // if(request.input('latest_fetch'))
    // {
    //   query.updated = { '$gt': request.input('latest_fetch') };
    // } 
 
     
    // let result = new Promise((resolve)=>{
    //     let rows = [] as any; 
    //     let total = 0;

    //     Scylla.models.instance.Form.eachRow(query, {materialized_view: 'updated_forms', fetchSize : 10,pageState : request.input('pageState')}, (n, row)=>{ 

    //       total = n+1;
    //       rows.push(row)
    //       // invoked per each row in all the pages
    //     }, (err, result)=>{
    //       // called once the page has been retrieved.
    //       if(err) throw err;
    //       // store the paging state
          
    //       resolve({pageState : result.pageState, data : rows, total})
    //   });
    // })
    // let form  = 
     

     
    

  }

  public async create ({}: HttpContextContract) {
  }
  
  public async leaderboard ({request,params}: HttpContextContract) {

    const limit = parseInt(request.input('limit')) || 10;

    let data = await Scylla.models.instance.Lead.findAsync({form_id: {'$eq' : params.id},   $orderby: { '$desc' :'point' }, $limit : limit}, { materialized_view: 'leaderboard', raw: true});
    
    return data;
   
  }

  public async store ({request}: HttpContextContract) {
     
    let data = request.all(); 
    
    data.id = Scylla.models.uuid();

    data.header = JSON.stringify(data.header);

    data.response_fields = JSON.stringify(data.response_fields)

    data.list_field = JSON.stringify(data.list_field);

    data.trx_status = JSON.stringify(data.trx_status); 

    data.created = Date.now().toString();

    data.updated = Date.now().toString();   

    let formOwner = new Scylla.models.instance.FormOwner({form_id : data.id.toString(), user_id : data.user_id, role : "owner"})
    
    await formOwner.saveAsync()

    let form = new Scylla.models.instance.Form(data)
    
    await form.saveAsync()

    await Scylla.models.instance.FormOwner.findOneAsync({form_id : data.id.toString(), user_id : data.user_id});  

    return data;
  }
 

  public async show ({params, auth, response}: HttpContextContract) {

    if(!auth.user)
    {
      return response.abort("Tidak terauthentikasi")
    } 
 
    let formOwner = await Scylla.models.instance.FormOwner.findOneAsync({form_id : params.id, user_id : auth.user.id.toString()}); 
 

    if(!formOwner)
    {
      return response.abort("Tidak terauthentikasi")
    }

    let form = await Scylla.models.instance.Form.findOneAsync({id : Scylla.models.uuidFromString(params.id)},{raw : true}); 
 
 
 
    form.header = JSON.parse(form.header)

    form.response_fields = JSON.parse(form.response_fields)
 
    form.list_field = JSON.parse(form.list_field)

    if(form.trx_status)
    {
      form.trx_status = JSON.parse(form.trx_status)
    }else{
      if(form.is_ecommerce)
      {
        form.trx_status = ["dipesan", "dibayar", "diproses", "dikirim", "selesai"]
      }else{
        form.trx_status = ['submit']
      }
    }
    

    return form;
  }

  public async edit ({}: HttpContextContract) {

  }

  public async update ({params,request}: HttpContextContract) {
    
    let data = request.except(['id']); 
    
    data.header = JSON.stringify(data.header);

    data.list_field = JSON.stringify(data.list_field); 

    data.trx_status = JSON.stringify(data.trx_status); 

    data.response_fields = JSON.stringify(data.response_fields)

    data.complete_url = data.domain+'/'+data.slug;

    data.updated = Date.now().toString();

    data.lead_point = parseInt(data.lead_point);

    delete data.lead_number;


    await Scylla.models.instance.Form.update({id : Scylla.models.uuidFromString(params.id)},data);

    return 'ok'

  }

  public async checkSlug ({params}: HttpContextContract) {
    console.log(params.slug)
    return await Scylla.models.instance.Form.findOneAsync({
      slug : params.slug
    },{select : ['slug'],raw : true})
  

    
  }

  public async destroy ({params}: HttpContextContract) {
    Scylla.models.instance.Form.delete({
    id : Scylla.models.uuidFromString(params.id)
  })


  var leads =  await Scylla.models.instance.Lead.findAsync({form_id:  params.id },{ })

  leads.forEach(element => {
      element.delete();
  });

  return 'ok'
}



  
}
