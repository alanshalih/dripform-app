import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Scylla from 'App/Services/Scylla'

export default class WhatsappDevicesController {
  public async index ({request}: HttpContextContract) {
    return Scylla.models.instance.WhatsappDevice.findAsync({form_id : request.input("form_id")})
  }

  public async create ({}: HttpContextContract) {
  }

  public async store ({request}: HttpContextContract) {
    let reqData = request.all();

    reqData.id = Scylla.models.uuid();
    
    let data = new Scylla.models.instance.WhatsappDevice(reqData);
    
    data.save();

    return reqData;

  }

  public async show ({}: HttpContextContract) {
  }

  public async edit ({}: HttpContextContract) {
  }

  public async update ({request}: HttpContextContract) {
    Scylla.models.instance.WhatsappDevice.update({id : Scylla.models.uuidFromString(request.input('id')), form_id : request.input("form_id")},request.except(['id',"form_id"]))
  }

  public async destroy ({params}: HttpContextContract) {
    Scylla.models.instance.WhatsappDevice.findOne({id: Scylla.models.uuidFromString(params.id)}, function(err, data){
      if(err) throw err;
  
      //Note that returned variable john here is an instance of your model,
      //so you can do john.delete() like the following
      if(data)
      data.delete(function(err){
          //...
          if(err) throw err;
      });
  });
  }
}
