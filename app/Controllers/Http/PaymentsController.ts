import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Scylla from 'App/Services/Scylla';
const crypto = require('crypto'); 
import {TriggerWAAutomation,TriggerEmailAutomation} from '../../Function/TriggerAutomation'

export default class PaymentsController {

    

    public async tripaySubmit ({auth,request}: HttpContextContract) {
        if(!auth.user)
        {
            return {};
        }
        let data = request.all();

        delete data.user_id;
        
        await Scylla.models.instance.Tripay.update({user_id : auth.user.id},data);

         return 'ok'

    }
    public async tripay ({auth}: HttpContextContract) {

        if(!auth.user)
        {
            return {};
        }

        let tripay = await Scylla.models.instance.Tripay.findOneAsync({user_id : auth.user.id});

        if(tripay)
        {
            return tripay;
        }else{
            tripay = {user_id : auth.user.id, urlCallback : process.env.APP_URL+'/tripay/callback'};
            
            let result =  new Scylla.models.instance.Tripay(tripay);

            result.save();
            
            return tripay;


        }
    }

    public async tripayCallback ({request}: HttpContextContract) {

        var data = request.all();   

        if(!data.reference)
        {
            return 'no reference';
        }

        if(data.status != 'PAID')
        {
            return 'status not paid';
        }

        let transaction = await Scylla.models.instance.TripayTransaction.findOneAsync({"reference" : data.reference})

        if(transaction)
        {
         
            if(transaction.privateKey)
            {
                const HmacSHA256 = crypto.createHmac("sha256", transaction.privateKey);

                var signature = HmacSHA256.update(JSON.stringify(data)).digest("hex");

                let callbackSignature = request.header('X-Callback-Signature');

                if(callbackSignature != signature)
                {
                    return 'signature unmatched';
                } 
                 
            }

           let lead =  await Scylla.models.instance.Lead.findOneAsync({"id" : transaction.lead_id});

           if(lead)
           {
               lead.is_paid = true;
               lead.paid_at = Date.now().toString();
               lead.status = 'dibayar';
               lead.updated = Date.now().toString();;
               let all_columns = JSON.parse(lead.all_columns);
               all_columns.status = 'dibayar';
               lead.all_columns = JSON.stringify(all_columns)
               lead.save();

               if(lead.whatsapp_device_id)
               TriggerWAAutomation(lead,all_columns)
                
               if(lead.email)
               TriggerEmailAutomation(lead,all_columns)
           }
        }else{
            return 'no transaction';
        }

       

      

     

      
         
    }

    

    public async mootaSubmit ({auth,request}: HttpContextContract) {
        if(!auth.user)
        {
            return {};
        }
        let data = request.all();

        delete data.user_id;
        
        await Scylla.models.instance.BankToken.update({user_id : auth.user.id},data);

         return 'ok'

    }
    public async moota ({auth}: HttpContextContract) {

        if(!auth.user)
        {
            return {};
        }

        let moota = await Scylla.models.instance.BankToken.findOneAsync({user_id : auth.user.id});

        if(moota)
        {
            return moota;
        }else{
            moota = {user_id : auth.user.id, token : (Math.random() + 1).toString(36).substring(2),  urlCallback : process.env.APP_URL+'/moota/callback'};
            
            let result =  new Scylla.models.instance.BankToken(moota);

            result.save();
            
            return moota;


        }
    }

    public async mootaCallback ({request}: HttpContextContract) {

        var list_mutasi = request.body() as any;   
 
        for await (const mutasi of list_mutasi) {

            let transaction = await Scylla.models.instance.BankTransaction.findOneAsync({"price" : parseInt(mutasi.amount), bank_account : mutasi.account_number.toString()},{})

            if(transaction)
            {
              
                if(transaction.token_id)
                {
                    const HmacSHA256 = crypto.createHmac("sha256", transaction.token_id);

                    var signature = HmacSHA256.update(JSON.stringify(list_mutasi)).digest("hex");

                    let callbackSignature = request.header('signature');
                

                    if(callbackSignature == signature)
                    {
                        let lead =  await Scylla.models.instance.Lead.findOneAsync({"id" : transaction.lead_id});

                       if(lead)
                       {
                           lead.is_paid = true;
                           lead.paid_at = Date.now().toString();
                           lead.status = 'dibayar';
                           lead.updated = Date.now().toString();;
                           let all_columns = JSON.parse(lead.all_columns);
                           all_columns.status = 'dibayar';
                           lead.all_columns = JSON.stringify(all_columns)
                           lead.save();

                           transaction.delete()

                           if(lead.whatsapp_device_id)
                            TriggerWAAutomation(lead,all_columns)
                            
                            if(lead.email)
                            TriggerEmailAutomation(lead,all_columns)

                           


                           
                       }
                    } 
                }
            }

        }


        return 'ok'; 
         
    }



}
