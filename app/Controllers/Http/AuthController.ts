import Redis from '@ioc:Adonis/Addons/Redis';
import Hash from '@ioc:Adonis/Core/Hash'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import View from '@ioc:Adonis/Core/View'; 
import axios from 'axios' 
import Env from '@ioc:Adonis/Core/Env'
import Scylla from 'App/Services/Scylla';
export default class AuthController {

    public async register ({request,auth,response}: HttpContextContract) {

        const user = request.only(["email","password","name","id"]);

        var data =  await Scylla.models.instance.User.findOneAsync({email:  user.email },{raw:true})

        if(data)
        {
            return response.abort("Email telah digunakan",409);
        }

        user.id = Scylla.models.uuid();
        
        user.password = await Hash.make(user.password)
  
        let getUser = await new Scylla.models.instance.User(user); 

        await getUser.saveAsync();

        await auth.use('web').loginViaId(user.id.toString())  
        
        delete user.password;

        return user;
    }

    public async registerFromLP ({request,auth,response}: HttpContextContract) {

        const user = request.only(["email","password","name","id"]);

        var data =  await Scylla.models.instance.User.findOneAsync({email:  user.email },{raw:true})

        if(data)
        {
            return response.abort("Email telah digunakan",409);
        }

        user.id = Scylla.models.uuid();
        
        user.password = await Hash.make(user.password)
  
        let getUser = await new Scylla.models.instance.User(user); 

        await getUser.saveAsync();

        await auth.use('web').loginViaId(user.id.toString())  
        
         
        return response.redirect("/");
    }

    

    public async logout({auth}: HttpContextContract) {
        await auth.use('web').logout()
        return 'ok'
    }

    

    public async login ({request,auth,response}: HttpContextContract) {
        const email = request.input('email')
        const password = request.input('password')
 
        var data =  await Scylla.models.instance.User.findOneAsync({email:  email },{raw:true})

        if(!data)
        {
            return response.abort("Email tidak ditemukan",404);
        }
     
        await auth.attempt(email, password, true)  


        return data;;
    }

    

    public async resetpassword ({request,response}: HttpContextContract) {
        const email = request.input('email') 

        // Lookup user manually
        const user = await Scylla.models.instance.User.findOneAsync({email : email},{})

        // Verify password
       
        if(user)
        {
            let code = Math.random().toString(36).substr(3,35);

            await Redis.setex("reset-password:"+code,3600,user.id);
            
            const html = await View.render('email/forgot-password', {
                name: user.name,
                unsubscribe_url : Env.get('APP_URL')+'/unsubscribe',
                RESET_URL : Env.get('APP_URL')+'/reset-password/'+code
              })

              axios.post('http://email.maxgrabb.com:2019/send-bulk',{data : [{
                  from : 'Reset Password Drip <arief@maxgrabb.com>',
                  to : user.email,
                  subject : 'Reset Password Drip',
                  html : html,
                  transactional : true
              }], "send_from" : "maxgrabb.com"})

              return 'ok'
        }else{
            return   response.badRequest('Email Not Found');
        }

       
    }

    

    public async verifyReset ({params,response}: HttpContextContract) {
    
 

        // Verify password
       
        if(params.id)
        {
           

            const user_id = await Redis.get("reset-password:"+params.id);
            
            if(user_id)
            {
                return 'OK';
            }else{
                return   response.badRequest('Cridential Not Found');
            }
        }else{
            return   response.badRequest('Cridential Not Found');
        }

       
    }

    public async makePassword ({request,response,auth}: HttpContextContract) {
    
 

        // Verify password
       
        if(request.input('id'))
        {
           

            const user_id = await Redis.get("reset-password:"+request.input('id'));
            
            if(user_id)
            {
                const user = await Scylla.models.instance.User.findOneAsync({id : Scylla.models.uuidFromString(user_id)})
                if(user)
                {
                    user.password = await Hash.make(request.input('password'));
                    
                    user.save();

                    await auth.use('web').loginViaId(user.id.toString())

                    await Redis.del("reset-password:"+request.input('id'))

                    return user;
                }else{
                    return   response.badRequest('Cridential Not Found');
                }
            }else{
                return   response.badRequest('Cridential Not Found');
            }
        }else{
            return   response.badRequest('Cridential Not Found');
        }

       
    }

    public async googleRedirect ({ally,auth,response}: HttpContextContract)  {
        const google = ally.use('google')
      
        /**
         * User has explicitly denied the login request
         */
        if (google.accessDenied()) {
          return 'Access was denied'
        }
      
        /**
         * Unable to verify the CSRF state
         */
        if (google.stateMisMatch()) {
          return 'Request expired. Retry again'
        }
      
        /**
         * There was an unknown error during the redirect
         */
        if (google.hasError()) {
          return google.getError()
        }
      
        /**
         * Finally, access the user
         */
        const googleuser = await google.user()
 
        let GetUser = await Scylla.models.instance.User.findOneAsync({email : googleuser.email},{raw:true})
 
  
        if(GetUser)
        {
            
            await auth.use('web').loginViaId(GetUser.id.toString())

            return response.redirect('/')

        }else{
             
            let user = {
                id : Scylla.models.uuid(),
                name : googleuser.name,
                email : googleuser.email,
                password : Math.random().toString(26)
            }; 

            user.password = await Hash.make(user.password)
      
            let getUser = await new Scylla.models.instance.User(user); 
    
            await getUser.saveAsync();

            await auth.use('web').loginViaId(user.id.toString())   

            return response.redirect('/')
    
        }
      
      }
 
    
}
