import Redis from '@ioc:Adonis/Addons/Redis';
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Scylla from 'App/Services/Scylla'; 
import {TriggerWAAutomation,TriggerEmailAutomation} from '../../Function/TriggerAutomation'


export default class LeadsController {
  public async index ({params,request}: HttpContextContract) {

    let query = {form_id: {'$eq' : params.form_id}} as any;

    if(request.input('latest_fetch'))
    {
      query.updated = { '$gt': request.input('latest_fetch') };
    } 

  
    let result = new Promise((resolve)=>{
      let rows = [] as any; 
      let total = 0;

      Scylla.models.instance.Lead.eachRow(query, {materialized_view: 'updated_leads', fetchSize : 100,pageState : request.input('pageState')}, (n, row)=>{ 

        total = n+1;
        row.all_columns = JSON.parse(row.all_columns)
        rows.push(row)
        // invoked per each row in all the pages
      }, (err, result)=>{
        // called once the page has been retrieved.
        if(err) throw err;
        // store the paging state
        
        resolve({pageState : result.pageState, data : rows, total})
    });
  })


    
    return result;
  }

  public async create ({}: HttpContextContract) {
  }

  public async store ({}: HttpContextContract) {
  }

  public async show ({params}: HttpContextContract) {
    let lead =  await Scylla.models.instance.Lead.findOneAsync({id : params.id})
    lead.all_columns = JSON.parse(lead.all_columns)
    return lead;
  }

  public async edit ({}: HttpContextContract) {
  }

  public async updateStatus ({request}: HttpContextContract) {

   
     

    let lead = await Scylla.models.instance.Lead.findOneAsync({id : request.input('id')})
  
    lead.status = request.input("status");

    
    let all_columns = JSON.parse(lead.all_columns);
    all_columns.status = lead.status;
    lead.all_columns = JSON.stringify(all_columns)
    lead.updated = Date.now().toString();;

    if(lead.status == 'dibayar')
    {
      lead.paid_at = Date.now().toString();
      lead.is_paid = true;
    } 
    
    lead.save();
    lead.all_columns = all_columns;

    if(lead.whatsapp_device_id)
    TriggerWAAutomation(lead,all_columns)
     
    if(lead.email)
    TriggerEmailAutomation(lead,all_columns)

    return lead;

  }

  public async update ({params,request}: HttpContextContract) {

    let lead = await Scylla.models.instance.Lead.findOneAsync({id : params.id})

    let all_columns = request.input("all_columns");
    lead.all_columns = JSON.stringify(all_columns)



    lead.name = all_columns.name;
    lead.email = all_columns.email;
    lead.phone = all_columns.phone;
    lead.point = parseInt(request.input("point"));
    lead.updated = Date.now().toString();;

    Redis.set("point:" + lead.id,lead.point);

    lead.save()

    return 'ok'


  }

  public async destroy ({}: HttpContextContract) {
  }

  public async downline ({params}: HttpContextContract) {

    return await Scylla.models.instance.Lead.findAsync({upline_id : params.id})
  }

  
}
