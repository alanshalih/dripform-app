var models = require('express-cassandra'); 
require('dotenv').config() 
//Tell express-cassandra to use the models-directory, and
//use bind() to load the models using cassandra configurations.


  let host = process.env.SCYLLA_HOST;
  let array_host;

  if(host)
  {
      array_host = host.split(',');
  }
 
  let clientOptions = {
      contactPoints: array_host,
      localDataCenter: process.env.SCYLLA_DC, 
      keyspace: process.env.SCYLLA_KEYSPACE,
      queryOptions: {consistency: models.consistencies.one}
  }
 
function boot(callback){  

    models.setDirectory( __dirname + '/../Models').bind(
        {
            clientOptions: clientOptions,
            ormOptions: {
                defaultReplicationStrategy: {
                    class: 'SimpleStrategy',
                    replication_factor: array_host.length,
                  },
                  disableTTYConfirmation : true,
                  migration: 'safe',
                  createKeyspace: false,
            }
        },
        function(err) {

            if(callback)
            {
                callback();
            }
            
            if(err) throw err;

         

    
            // You'll now have a `person` table in cassandra created against the model
            // schema you've defined earlier and you can now access the model instance
            // in `models.instance.Person` object containing supported orm operations.
           
        }
    );
    
}
function migration(){ 
    


  models.setDirectory( __dirname + '/../Models').bind(
      {
          clientOptions: clientOptions,
          ormOptions: {
              defaultReplicationStrategy: {
                  class: 'SimpleStrategy',
                  replication_factor: array_host.length,
                },
                disableTTYConfirmation : false,
                migration: 'alter',
                createKeyspace: true,
          }
      },
      function(err) {
          if(err) throw err;
  
          // You'll now have a `person` table in cassandra created against the model
          // schema you've defined earlier and you can now access the model instance
          // in `models.instance.Person` object containing supported orm operations.
         
      }
  );
  
}
 
function development(){ 
    


    models.setDirectory( __dirname + '/../Models').bind(
        {
            clientOptions: clientOptions,
            ormOptions: {
                defaultReplicationStrategy: {
                    class: 'SimpleStrategy',
                    replication_factor: array_host.length,
                  },
                  disableTTYConfirmation : true,
                  migration: 'alter',
                  createKeyspace: true,
            }
        },
        function(err) {
            if(err) throw err;
    
            // You'll now have a `person` table in cassandra created against the model
            // schema you've defined earlier and you can now access the model instance
            // in `models.instance.Person` object containing supported orm operations.
           
        }
    );
    
  }
   
  
  export default {models,boot,migration,development}