const cassandra = require('cassandra-driver');

let host = process.env.SCYLLA_HOST;
let array_host;

if(host)
{
    array_host = host.split(',');
}


const client = new cassandra.Client({
  contactPoints: array_host,
  localDataCenter: process.env.SCYLLA_DC, 
  keyspace: process.env.SCYLLA_KEYSPACE,
});

export default client;