 
import axios from "axios";
import Scylla from "./app/Services/Scylla"

Scylla.boot(function(){
    var query = {
        send_at:{
            '$token':{'$lt': Date.now().toString()}
        }
    };
    Scylla.models.instance.EmailFollowupQueue.find(query,function(err,data){
        if(err)
        {
            throw err;
        } 
      
        if(data.length)
        {
            let emails = [] as any;
            data.forEach((item,index)=>{

              
             
                Scylla.models.instance.Lead.findOne({id : item.lead_id},{raw : true,select : ["id","status"]},function(err,lead){
                    if(err) throw err;
    
                    if(lead.status == item.status)
                    {

                        emails.push({
                            "from" : item.sender_name+" <"+item.sender_email+">",
                            "to" : item.lead_email,
                            "transactional" : true,
                            "subject" : item.subject,
                            "html" : item.content
                        });
                        
                          
                    }   



                
                    item.delete(function(err){
                        if(err) throw err;
    
                        if(index == data.length-1)
                        {
                            axios.post(" http://172.104.180.206:2019/send-bulk",{
                                "data" : emails,
                                "send_from" : "drip.id"
                            }).then(()=>{
                                setTimeout(()=>{
                                    process.exit(0)
                                },3000)
                            })

                            
                        } 
                    })  
                    
     
                })

                
            })
            
        }else{
            process.exit(0)
        }
        
    })

});

