module.exports = {
  purge: { 
    content: [ 
      './resources/src/**/*.vue'
    ],
  }, 
    darkMode: false, // or 'media' or 'class'
    theme: {
      extend: {
        zIndex: {
          '-1': '-1'
        },
        flexGrow: {
          5: '5'
        },
        maxHeight: {
          'screen-menu': 'calc(100vh - 3.5rem)',
          modal: 'calc(100vh - 160px)'
        }
      }
    },
    variants: {
      extend: {}
    },
    plugins: []
  }