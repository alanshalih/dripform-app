import type { HashContract } from '@ioc:Adonis/Core/Hash'
import type {
    UserProviderContract,
    ProviderUserContract
} from '@ioc:Adonis/Addons/Auth'
import Scylla from 'App/Services/Scylla'

export type User = {
    id: string
    email: string
    password: string
    rememberMeToken: string | null
  }

  export type ScyllaDbAuthProviderConfig = {
    driver: 'scylla'
  }
  
  /**
   * Provider user works as a bridge between your User provider and
   * the AdonisJS auth module.
   */
  class ProviderUser implements ProviderUserContract<User> {
    constructor(public user: User | null, private hash: HashContract) {}
  
    public getId() {
      return this.user ? this.user.id : null
    }
  
    public getRememberMeToken() {
      return this.user ? this.user.rememberMeToken : null
    }
  
    public setRememberMeToken(token: string) {
      if (!this.user) {
        return
      }
      this.user.rememberMeToken = token
    }
  
    public async verifyPassword(plainPassword: string) {
      if (!this.user) {
        throw new Error('Cannot verify password for non-existing user')
      }
  
      return this.hash.verify(this.user.password, plainPassword)
    }
  }
  
  /**
   * The User provider implementation to lookup a user for different
   * operations
   */
  export class ScyllaDbAuthProvider implements UserProviderContract<User> {
    constructor(
      public config: ScyllaDbAuthProviderConfig,
      private hash: HashContract
    ) {}
  
    public async getUserFor(user) {
      return new ProviderUser(user, this.hash)
    }

    public async getUser(query){
        return new Promise((resolve, reject) => {
            Scylla.models.instance.User.findOne(query,{raw:true}, function(err, people){
                if(err)
                {
                    reject(err);
                    throw err;
                } 
 
                //people is an array of model instances containing the persons with name `John`
                resolve(people)
            });
            
          
        })
      }
  
    public async updateRememberMeToken(user: ProviderUser) { 
        new Scylla.models.instance.User({
            id: user.getId(),
            rememberMeToken: user.getRememberMeToken() 
        }); 
    }
  
    public async findById(id: string | number) { 

  
 
          const user = await this.getUser({id: Scylla.models.uuidFromString(id) });

 
          return this.getUserFor(user || null);
      
          
 
    }

    public async findByUid(uidValue: string) {
      
 
  
          const user = await this.getUser({email: uidValue});
          
          return this.getUserFor(user || null);


      }
  
 
  
    public async findByRememberMeToken(userId: string | number, token: string) { 

      const user = await this.getUser({id: userId, rememberMeToken : token});
          
      return this.getUserFor(user || null);
 
    }
  }