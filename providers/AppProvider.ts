import { ApplicationContract } from '@ioc:Adonis/Core/Application' 
import Scylla from 'App/Services/Scylla'

export default class AppProvider {
  constructor (protected app: ApplicationContract) {
  }

  public register () {
    // Register your own bindings
  }

  public async boot () {
    // IoC container is ready

    if(process.env.NODE_ENV == 'development')
    {
      console.log("development state")
      Scylla.development();
    }else{
      Scylla.boot(()=>{
        console.log("database boot")
      });
    }

    // Scylla.boot(()=>{
    //   console.log("database boot")
    // });
  

    const Auth = this.app.container.resolveBinding('Adonis/Addons/Auth')
    const Hash = this.app.container.resolveBinding('Adonis/Core/Hash')

    const { ScyllaDbAuthProvider } = await import('./ScyllaDbAuthProvider')

    Auth.extend('provider', 'scylla', (_, __, config) => {
      return new ScyllaDbAuthProvider(config, Hash)
    })

  }

  public async ready () {
    // App is ready 
  }

  public async shutdown () {
    // Cleanup, since app is going down
  }
}
